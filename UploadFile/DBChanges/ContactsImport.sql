USE [ContactsImport]
GO
/****** Object:  StoredProcedure [dbo].[CRM_Getdata]    Script Date: 11/5/2016 11:58:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Aspiore Software Consultancy>
-- Create date: <07-09-2015>
-- Description:	<Retrive Import Data Showing Users >
-- =============================================
ALTER PROCEDURE [dbo].[CRM_Getdata]	
AS
BEGIN
	SELECT RecordID, FirstName, MiddleName, LastName, SpouseName, Title, Suffix, Initials, WebPage,convert(nvarchar(10), Birthday,103) as Birthday,convert(nvarchar(10),Anniversary,103) as Anniversary, Notes, EmailAddress, Email2Address, Email3Address, 
           HomePhone, HomePhoneTwo, HomeFax, MobilePhone, HomeAddressOne, HomeAddressTwo, HomeCity, HomeState,                HomeZip, BusinessPhone, BusinessPhoneTwo, BusinessFax, BusinessAddressOne, BusinessAddressTwo,                     BusinessCity, BusinessState, BusinessPostalCode, BusinessCounty,Categories 	,HPrimeID
			,GroupID
FROM       ZZ_CMSImport_Temp  Order BY RecordID Desc
END
