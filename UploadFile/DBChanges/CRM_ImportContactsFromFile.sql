USE [ContactsImport]
GO
/****** Object:  StoredProcedure [dbo].[CRM_ProcessimportAction]    Script Date: 11/5/2016 11:58:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [dbo].[CRM_ProcessimportAction]
   @RecordID int
, @importgroupID int
AS
Begin


	--print 'ImportGroupID'
	--print @importgroupID
	--print '---------'


	SET NOCOUNT ON;
	declare @Exists int

	declare @ContactFirstName  nvarchar(300)
	declare @ContactLastname nvarchar(300)
	Declare @RecordPrimeId int


	declare @EmailAddressTypeID  INT = 1
	declare @EmailAddress2TypeID  INT = 2
	declare @EmailAddress3TypeID  INT = 3
	declare @EmailAddress4TypeID  INT = 4
	declare @EmailAddress5TypeID  INT = 5

	declare @HomeAddressTypeID  INT = 1
	declare @BusinessAddressTypeID  INT = 2
	declare @OtherAddressTypeID  INT = 3

	declare @BirthDayTypeID INT = 1
	declare @AnniversaryTypeID INT = 4


	declare @HomePhoneTypeID  INT = 2	
	declare @HomePhone2TypeID  INT = 2	
	declare @BusinessPhoneTypeID  INT = 3
	declare @BusinessPhone2TypeID  INT = 3
	declare @OtherAddressPhoneTypeID  INT = 7
	declare @OtherAddressPhone2TypeID  INT = 7

	
	declare @PrimaryPhoneTypeID  INT = 1
	declare @MobilePhoneTypeID  INT = 1
	declare @MobilePhone2TypeID  INT = 5
	declare @OtherPhoneTypeID  INT = 6
	
	declare @HomeFaxTypeID  INT = 4
	declare @BusinessFaxTypeID  INT = 4
	declare @OtherAddressFaxTypeID  INT = 4

		declare @GroupID int

	SELECT @ContactFirstName = gs.FirstName, @ContactLastname =  gs.LastName ,  @RecordPrimeId =  gs.HPrimeID, @GroupID = GroupID
	FROM ZZ_CMSImport_Temp gs where gs.RecordID = @RecordID


	print @ContactFirstName
	print @ContactLastname
	print @RecordID
	print @RecordPrimeId

	set @Exists = (select COUNT(acm.ContactID) from AgentContacts_Main acm 
	where acm.ContactFirstName = @ContactFirstName 
	and acm.ContactLastName = @ContactLastname 
	and acm.HPAgentID = @RecordPrimeId
	and isnull(acm.Active,1) = 1)
 
 


 	DECLARE @CurRecord table(  
		ContactFirstName nvarchar(500) NULL,  
		ContactLastname nvarchar(500) NULL,  
		RecordPrimeId int NULL,  
		GroupID int NULL,
		ContactID int NULL,
		[EmailAddress] [nvarchar](500) NULL,
		[Email2Address] [nvarchar](500) NULL,
		[Email3Address] [nvarchar](500) NULL,
		[HomePhone] [nvarchar](500) NULL,
		[HomePhoneTwo] [nvarchar](500) NULL,
		[MobilePhone] [nvarchar](500) NULL,
		[BusinessPhone] [nvarchar](500) NULL,
		[BusinessPhoneTwo] [nvarchar](500) NULL,	
		[HomeFax] [nvarchar](500) NULL,
		[HomeAddressOne] [nvarchar](500) NULL,
		[BusinessAddressOne] [nvarchar](500) NULL,
		[Birthday] [nvarchar](500) NULL,
		[Anniversary] [nvarchar](500) NULL,

		[BusinessFax] [nvarchar] (500) NULL,
		[Email4Address] [nvarchar](500) NULL,
		[Email5Address] [nvarchar](500) NULL,
		[MobilePhoneTwo] [nvarchar](500) NULL,	
		[OtherPhone] [nvarchar](500) NULL,		
		[OtherAddressOne] [nvarchar](500) NULL,
		[OtherAddressPhone] [nvarchar](500) NULL,
		[OtherAddressPhoneTwo] [nvarchar](500) NULL,
		[OtherAddressFax] [nvarchar](500) NULL

		);
	 
	 INSERT INTO @CurRecord (ContactFirstName, ContactLastname,RecordPrimeId,GroupID,ContactID,EmailAddress,Email2Address,Email3Address,HomePhone,HomePhoneTwo,
	 MobilePhone,BusinessPhone,BusinessPhoneTwo,HomeFax,HomeAddressOne,BusinessAddressOne,Birthday,Anniversary,
	 Email4Address,Email5Address,MobilePhoneTwo,OtherAddressOne,OtherAddressPhone,OtherAddressPhoneTwo,OtherAddressFax
	 )
	  SELECT FirstName, LastName, HPrimeID,GroupID,RecordID,EmailAddress,Email2Address,Email3Address,HomePhone,HomePhoneTwo,
	 MobilePhone,BusinessPhone,BusinessPhoneTwo,HomeFax,HomeAddressOne,BusinessAddressOne,Birthday,Anniversary,
	 Email4Address,Email5Address,MobilePhoneTwo,OtherAddressOne,OtherAddressPhone,OtherAddressPhoneTwo,OtherAddressFax
		FROM ZZ_CMSImport_Temp where RecordID = @RecordID
 
 

if @Exists > 0
begin
   print 'go for edit'
	declare @ExistingContactID int
	set @ExistingContactID = (select top 1 ContactID from AgentContacts_Main acm where acm.ContactFirstName 
= @ContactFirstName and acm.ContactLastName
 = @ContactLastname and acm.HPAgentID
 = @RecordPrimeId  and isnull(acm.Active,1) = 1)	
	
	--if (isnull((select GroupID from ZZ_CMSImport_Temp gs where gs.RecordID = @RecordID),0) <> 0)
	--	begin
	--	insert into dbo.AgentContacts_GroupMembership
	--	( ContactID, GroupID, InsertDate, HPrimeID, isActive)
	--	select @ExistingContactID, GroupID, getdate(), HPrimeID, 1 from ZZ_CMSImport_Temp gs where gs.RecordID = @RecordID
	--end

	UPDATE dbo.AgentContacts_Main 
		SET HPAgentID = IsNull(NULLIF(t1.HPrimeID,''), HPAgentID), 
		ContactTitle = IsNull(NULLIF(t1.Title,''), ContactTitle), 
		ContactFirstName = IsNull(NULLIF(t1.FirstName,''), ContactFirstName), 
		ContactMiddleName =IsNull(NULLIF(t1.MiddleName,''), ContactMiddleName), 
		ContactLastName = IsNull(NULLIF(t1.LastName,''), ContactLastName), 
		ContactSuffix = IsNull(NULLIF(t1.Suffix,''), ContactSuffix), 
		ContactDescription =IsNull(NULLIF(t1.Notes,''), ContactDescription),
		Active = 1	
	FROM (
		select 
			gs1.HPrimeID, gs1.Title, gs1.FirstName, gs1.MiddleName, gs1.LastName, gs1.Suffix,  Notes
		from ZZ_CMSImport_Temp gs1 where gs1.RecordID = @RecordID)  t1  where ContactID = @ExistingContactID
		
		-- Email One
		if (isnull((select  nullif(EmailAddress,'') from @CurRecord),'0') <> '0')
		begin
		IF EXISTS (SELECT 1 from AgentContacts_EmailAddresses where ContactID = @ExistingContactID and EmailTypeID = @EmailAddressTypeID)
		BEGIN
			update dbo.AgentContacts_EmailAddresses set
			ContactEmailAddress = t1.EmailAddress	
			 FROM
			(select EmailAddress from ZZ_CMSImport_Temp temp1 where temp1.RecordID = @RecordID) t1
			where ContactID = @ExistingContactID and EmailTypeID = @EmailAddressTypeID
			END
			ELSE
			BEGIN
			INSERT INTO AgentContacts_EmailAddresses (ContactID, ContactEmailAddress, EmailTypeID, isPreferred)
			values (@ExistingContactID, (select EmailAddress from ZZ_CMSImport_Temp where RecordID = @RecordID), @EmailAddressTypeID,1)
			END
			
		end
		
		-- Email Two
		if (isnull((select nullif(Email2Address,'') from @CurRecord),'0') <> '0')
		begin
		IF EXISTS (SELECT 1 from AgentContacts_EmailAddresses where ContactID = @ExistingContactID and EmailTypeID = @EmailAddress2TypeID)
		BEGIN
			update dbo.AgentContacts_EmailAddresses set	
			ContactEmailAddress = t2.Email2Address			
			FROM (select Email2Address from ZZ_CMSImport_Temp temp2 where temp2.RecordID = @RecordID) t2
			 where ContactID = @ExistingContactID and EmailTypeID = @EmailAddress2TypeID
			 END
			ELSE
			BEGIN
			INSERT INTO AgentContacts_EmailAddresses (ContactID, ContactEmailAddress, EmailTypeID, isPreferred)
			values (@ExistingContactID, (select EmailAddress from ZZ_CMSImport_Temp where RecordID = @RecordID), @EmailAddress2TypeID, 0)
			END
		end
		-- Email Three
		if (isnull((select nullif(Email3Address,'') from @CurRecord),'0') <> '0')
		begin
		IF EXISTS (SELECT 1 from AgentContacts_EmailAddresses where ContactID = @ExistingContactID and EmailTypeID = @EmailAddress3TypeID)
		BEGIN
			update dbo.AgentContacts_EmailAddresses set
			ContactEmailAddress = t1.Email3Address		
			FROM (select  Email3Address from ZZ_CMSImport_Temp temp3 where temp3.RecordID = @RecordID) t1
			 where ContactID = @ExistingContactID and EmailTypeID = @EmailAddress3TypeID
			  END
			ELSE
			BEGIN
			INSERT INTO AgentContacts_EmailAddresses (ContactID, ContactEmailAddress, EmailTypeID, isPreferred)
			values (@ExistingContactID, (select EmailAddress from ZZ_CMSImport_Temp where RecordID = @RecordID), @EmailAddress3TypeID, 0)
			END
		end


		-- Email Four
		if (isnull((select nullif(Email4Address,'') from @CurRecord),'0') <> '0')
		begin
		IF EXISTS (SELECT 1 from AgentContacts_EmailAddresses where ContactID = @ExistingContactID and EmailTypeID = @EmailAddress4TypeID)
		BEGIN
			update dbo.AgentContacts_EmailAddresses set
			ContactEmailAddress = t1.Email4Address		
			FROM (select  Email4Address from ZZ_CMSImport_Temp temp3 where temp3.RecordID = @RecordID) t1
			 where ContactID = @ExistingContactID and EmailTypeID = @EmailAddress4TypeID
			  END
			ELSE
			BEGIN
			INSERT INTO AgentContacts_EmailAddresses (ContactID, ContactEmailAddress, EmailTypeID, isPreferred)
			values (@ExistingContactID, (select EmailAddress from ZZ_CMSImport_Temp where RecordID = @RecordID), @EmailAddress4TypeID, 0)
			END
		end


		-- Email Five
		if (isnull((select nullif(Email5Address,'') from @CurRecord),'0') <> '0')
		begin
		IF EXISTS (SELECT 1 from AgentContacts_EmailAddresses where ContactID = @ExistingContactID and EmailTypeID = @EmailAddress5TypeID)
		BEGIN
			update dbo.AgentContacts_EmailAddresses set
			ContactEmailAddress = t1.Email5Address		
			FROM (select  Email5Address from ZZ_CMSImport_Temp temp3 where temp3.RecordID = @RecordID) t1
			 where ContactID = @ExistingContactID and EmailTypeID = @EmailAddress5TypeID
			  END
			ELSE
			BEGIN
			INSERT INTO AgentContacts_EmailAddresses (ContactID, ContactEmailAddress, EmailTypeID, isPreferred)
			values (@ExistingContactID, (select EmailAddress from ZZ_CMSImport_Temp where RecordID = @RecordID), @EmailAddress5TypeID, 0)
			END
		end

				
		-- HomePhone
		if (isnull((select nullif(HomePhone,'') from @CurRecord),'0') <> '0')
		begin
		IF EXISTS (SELECT 1 from AgentContacts_PhoneNumbers where ContactID = @ExistingContactID and PhoneTypeID = @HomePhoneTypeID)
		BEGIN			
			update dbo.AgentContacts_PhoneNumbers set		
			ContactPhoneNumber = t1.HomePhone		
			FROM (select HomePhone from ZZ_CMSImport_Temp temp4 where temp4.RecordID = @RecordID) t1
			 where ContactID = @ExistingContactID and PhoneTypeID = @HomePhoneTypeID
			 END
			ELSE
			BEGIN
			INSERT INTO AgentContacts_PhoneNumbers (ContactID, ContactPhoneNumber, PhoneTypeID, isPreferred)
			values (@ExistingContactID, (select HomePhone from ZZ_CMSImport_Temp where RecordID = @RecordID), @HomePhoneTypeID, 0)
			END
		end
		-- HomePhoneTwo
		if (isnull((select nullif(HomePhoneTwo,'') from @CurRecord),'0') <> '0')
			begin
			IF EXISTS (SELECT 1 from AgentContacts_PhoneNumbers where ContactID = @ExistingContactID and PhoneTypeID = @HomePhone2TypeID)
		BEGIN		
			update dbo.AgentContacts_PhoneNumbers set		
			ContactPhoneNumber = t1.HomePhoneTwo		
			FROM (select  HomePhoneTwo from ZZ_CMSImport_Temp temp5 where temp5.RecordID = @RecordID) t1
			 where ContactID = @ExistingContactID and PhoneTypeID = @HomePhone2TypeID
			  END
			ELSE
			BEGIN
			INSERT INTO AgentContacts_PhoneNumbers (ContactID, ContactPhoneNumber, PhoneTypeID, isPreferred)
			values (@ExistingContactID, (select HomePhone from ZZ_CMSImport_Temp where RecordID = @RecordID), @HomePhone2TypeID, 0)
			END
		end

		-- Other Address Phone
		if (isnull((select nullif(OtherAddressPhone,'') from @CurRecord),'0') <> '0')
		begin
		IF EXISTS (SELECT * from AgentContacts_PhoneNumbers where ContactID = @ExistingContactID and PhoneTypeID = @OtherAddressPhoneTypeID)
		BEGIN			
			update dbo.AgentContacts_PhoneNumbers set		
			ContactPhoneNumber = t1.OtherAddressPhone		
			FROM (select OtherAddressPhone from ZZ_CMSImport_Temp temp4 where temp4.RecordID = @RecordID) t1
			 where ContactID = @ExistingContactID and PhoneTypeID = @OtherAddressPhoneTypeID
			 END
			ELSE
			BEGIN
			INSERT INTO AgentContacts_PhoneNumbers (ContactID, ContactPhoneNumber, PhoneTypeID, isPreferred)
			values (@ExistingContactID, (select OtherAddressPhone from ZZ_CMSImport_Temp where RecordID = @RecordID), @OtherAddressPhoneTypeID, 0)
			END
		end

		-- Other Address Phone Two
		if (isnull((select nullif(OtherAddressPhoneTwo,'') from @CurRecord),'0') <> '0')
			begin
			IF EXISTS (SELECT 1 from AgentContacts_PhoneNumbers where ContactID = @ExistingContactID and PhoneTypeID = @OtherAddressPhone2TypeID)
		BEGIN		
			update dbo.AgentContacts_PhoneNumbers set		
			ContactPhoneNumber = t1.OtherAddressPhoneTwo		
			FROM (select  OtherAddressPhoneTwo from ZZ_CMSImport_Temp temp5 where temp5.RecordID = @RecordID) t1
			 where ContactID = @ExistingContactID and PhoneTypeID = @OtherAddressPhone2TypeID
			  END
			ELSE
			BEGIN
			INSERT INTO AgentContacts_PhoneNumbers (ContactID, ContactPhoneNumber, PhoneTypeID, isPreferred)
			values (@ExistingContactID, (select OtherAddressPhoneTwo from ZZ_CMSImport_Temp where RecordID = @RecordID), @OtherAddressPhone2TypeID, 0)
			END
		end


		-- Mobile Phone
		if (isnull((select nullif(MobilePhone,'') from @CurRecord),'0') <> '0')
		begin
		IF EXISTS (SELECT 1 from AgentContacts_PhoneNumbers where ContactID = @ExistingContactID and PhoneTypeID = @MobilePhoneTypeID)
		BEGIN			
			update dbo.AgentContacts_PhoneNumbers set			
			ContactPhoneNumber = t1.MobilePhone			
			FROM (select MobilePhone from ZZ_CMSImport_Temp temp6 where temp6.RecordID = @RecordID) t1 
			 where ContactID = @ExistingContactID and PhoneTypeID = @MobilePhoneTypeID
			  END
			ELSE
			BEGIN
			INSERT INTO AgentContacts_PhoneNumbers (ContactID, ContactPhoneNumber, PhoneTypeID, isPreferred)
			values (@ExistingContactID, (select MobilePhone from ZZ_CMSImport_Temp where RecordID = @RecordID), @MobilePhoneTypeID, 1)
			END
		end

		-- Mobile Phone Two
		if (isnull((select nullif(MobilePhoneTwo,'') from @CurRecord),'0') <> '0')
		begin
		IF EXISTS (SELECT 1 from AgentContacts_PhoneNumbers where ContactID = @ExistingContactID and PhoneTypeID = @MobilePhone2TypeID)
		BEGIN			
			update dbo.AgentContacts_PhoneNumbers set		
			ContactPhoneNumber = t1.MobilePhoneTwo			
			FROM (select MobilePhoneTwo from ZZ_CMSImport_Temp temp6 where temp6.RecordID = @RecordID) t1 
			 where ContactID = @ExistingContactID and PhoneTypeID = @MobilePhone2TypeID
			  END
			ELSE
			BEGIN
			INSERT INTO AgentContacts_PhoneNumbers (ContactID, ContactPhoneNumber, PhoneTypeID, isPreferred)
			values (@ExistingContactID, (select MobilePhoneTwo from ZZ_CMSImport_Temp where RecordID = @RecordID), @MobilePhone2TypeID, 1)
			END
		end


		-- Other Phone
		if (isnull((select nullif(OtherPhone,'') from @CurRecord),'0') <> '0')
		begin
		IF EXISTS (SELECT 1 from AgentContacts_PhoneNumbers where ContactID = @ExistingContactID and PhoneTypeID = @OtherPhoneTypeID)
		BEGIN			
			update dbo.AgentContacts_PhoneNumbers set		
			ContactPhoneNumber = t1.OtherPhone			
			FROM (select OtherPhone from ZZ_CMSImport_Temp temp6 where temp6.RecordID = @RecordID) t1 
			 where ContactID = @ExistingContactID and PhoneTypeID = @OtherPhoneTypeID
			  END
			ELSE
			BEGIN
			INSERT INTO AgentContacts_PhoneNumbers (ContactID, ContactPhoneNumber, PhoneTypeID, isPreferred)
			values (@ExistingContactID, (select MobilePhoneTwo from ZZ_CMSImport_Temp where RecordID = @RecordID), @OtherPhoneTypeID, 1)
			END
		end



		-- Business Phone
		if (isnull((select nullif(BusinessPhone,'') from @CurRecord),'0') <> '0')
			begin
			IF EXISTS (SELECT 1 from AgentContacts_PhoneNumbers where ContactID = @ExistingContactID and PhoneTypeID = @BusinessPhoneTypeID)
		BEGIN		
			update dbo.AgentContacts_PhoneNumbers set		
			ContactPhoneNumber = t1.BusinessPhone		
		FROM	(select   BusinessPhone from ZZ_CMSImport_Temp temp7 where temp7.RecordID = @RecordID) t1
			 where ContactID = @ExistingContactID and PhoneTypeID = @BusinessPhoneTypeID
			 END
			ELSE
			BEGIN
			INSERT INTO AgentContacts_PhoneNumbers (ContactID, ContactPhoneNumber, PhoneTypeID, isPreferred)
			values (@ExistingContactID, (select BusinessPhone from ZZ_CMSImport_Temp where RecordID = @RecordID), @BusinessPhoneTypeID, 0)
			END
		end
		-- BusinessPhoneTwo
		if (isnull((select nullif(BusinessPhoneTwo,'') from @CurRecord),'0') <> '0')
			begin
			IF EXISTS (SELECT 1 from AgentContacts_PhoneNumbers where ContactID = @ExistingContactID and PhoneTypeID = @BusinessPhone2TypeID)
		BEGIN		
			update dbo.AgentContacts_PhoneNumbers set			
			ContactPhoneNumber = t1.BusinessPhoneTwo			
			FROM (select BusinessPhoneTwo from ZZ_CMSImport_Temp temp8 where temp8.RecordID = @RecordID) t1
			where ContactID = @ExistingContactID and PhoneTypeID = @BusinessPhone2TypeID
			 END
			ELSE
			BEGIN
			INSERT INTO AgentContacts_PhoneNumbers (ContactID, ContactPhoneNumber, PhoneTypeID, isPreferred)
			values (@ExistingContactID, (select BusinessPhoneTwo from ZZ_CMSImport_Temp where RecordID = @RecordID), @BusinessPhone2TypeID, 0)
			END
		end
		-- Home Fax
		if (isnull((select nullif(HomeFax,'') from @CurRecord),'0') <> '0')
			begin
			IF EXISTS (SELECT 1 from AgentContacts_PhoneNumbers where ContactID = @ExistingContactID and PhoneTypeID = @HomeFaxTypeID)
		BEGIN	
			update dbo.AgentContacts_PhoneNumbers set		
			ContactPhoneNumber = t1.HomeFax			
		FROM	(select HomeFax from ZZ_CMSImport_Temp temp9 where temp9.RecordID = @RecordID) t1
			where ContactID = @ExistingContactID and PhoneTypeID = @HomeFaxTypeID
			 END
			ELSE
			BEGIN
			INSERT INTO AgentContacts_PhoneNumbers (ContactID, ContactPhoneNumber, PhoneTypeID, isPreferred)
			values (@ExistingContactID, (select HomeFax from ZZ_CMSImport_Temp where RecordID = @RecordID), @HomeFaxTypeID, 0)
			END
		end
		-- Business Fax
		if (isnull((select nullif(BusinessFax,'') from @CurRecord),'0') <> '0')
			begin
			IF EXISTS (SELECT 1 from AgentContacts_PhoneNumbers where ContactID = @ExistingContactID and PhoneTypeID = @BusinessFaxTypeID)
		BEGIN	
			update dbo.AgentContacts_PhoneNumbers set		
			ContactPhoneNumber = t1.BusinessFax		
			FROM (select  BusinessFax from ZZ_CMSImport_Temp temp10 where temp10.RecordID = @RecordID) t1
			where ContactID = @ExistingContactID and PhoneTypeID = @BusinessFaxTypeID
			 END
			ELSE
			BEGIN
			INSERT INTO AgentContacts_PhoneNumbers (ContactID, ContactPhoneNumber, PhoneTypeID, isPreferred)
			values (@ExistingContactID, (select BusinessFax from ZZ_CMSImport_Temp where RecordID = @RecordID), @BusinessFaxTypeID, 0)
			END
		end
		
		-- Other Address Fax
		if (isnull((select nullif(OtherAddressFax,'') from @CurRecord),'0') <> '0')
			begin
			IF EXISTS (SELECT 1 from AgentContacts_PhoneNumbers where ContactID = @ExistingContactID and PhoneTypeID = @OtherAddressFaxTypeID)
		BEGIN	
			update dbo.AgentContacts_PhoneNumbers set		
			ContactPhoneNumber = t1.OtherAddressFax		
			FROM (select  OtherAddressFax from ZZ_CMSImport_Temp temp10 where temp10.RecordID = @RecordID) t1
			where ContactID = @ExistingContactID and PhoneTypeID = @OtherAddressFaxTypeID
			 END
			ELSE
			BEGIN
			INSERT INTO AgentContacts_PhoneNumbers (ContactID, ContactPhoneNumber, PhoneTypeID, isPreferred)
			values (@ExistingContactID, (select OtherAddressFax from ZZ_CMSImport_Temp where RecordID = @RecordID), @OtherAddressFaxTypeID, 0)
			END
		end


		-- Business Address
		if (isnull((select nullif(HomeAddressOne,'') from @CurRecord),'0') <> '0')
			begin
			IF EXISTS (SELECT 1 from AgentContacts_Addresses where ContactID = @ExistingContactID and AddressTypeID = @HomeAddressTypeID)
		BEGIN	
			update dbo.AgentContacts_Addresses set		
			StreetAddress = t1.StreetAddress,
			City = t1.HomeCity,
			State = t1.HomeState,
			Zip = t1.HomeState			
		FROM	(select isnull(HomeAddressOne,' ') + isnull(HomeAddressTwo,' ') as StreetAddress, HomeCity, HomeState, HomeZip from ZZ_CMSImport_Temp temp12 where temp12.RecordID = @RecordID) t1
			where ContactID = @ExistingContactID  and AddressTypeID = @HomeAddressTypeID
			 END
			ELSE
			BEGIN
			INSERT INTO AgentContacts_Addresses (ContactID, StreetAddress,City, State,Zip, AddressTypeID, isPreferred)
			values (@ExistingContactID,
			(select isnull(HomeAddressOne,' ') + isnull(HomeAddressTwo,' ') as StreetAddress from ZZ_CMSImport_Temp where RecordID = @RecordID),
			(select HomeCity from ZZ_CMSImport_Temp where RecordID = @RecordID),
			(select HomeState from ZZ_CMSImport_Temp where RecordID = @RecordID),
			(select HomeZip from ZZ_CMSImport_Temp where RecordID = @RecordID),@HomeAddressTypeID, 0)
			END
		end
		
		-- Business Address
		if (isnull((select nullif(BusinessAddressOne,'') from @CurRecord),'0') <> '0')
			begin
			IF EXISTS (SELECT 1 from AgentContacts_Addresses where ContactID = @ExistingContactID and AddressTypeID = @BusinessAddressTypeID)
		BEGIN	
			update dbo.AgentContacts_Addresses set		
			StreetAddress = t1.StreetAddress,
			City = t1.BusinessCity,
			State = t1.BusinessState,
			Zip = t1.BusinessPostalCode		
		FROM	(select isnull(BusinessAddressOne,' ') + isnull(BusinessAddressTwo,' ') as StreetAddress, BusinessCity, BusinessState, BusinessPostalCode from ZZ_CMSImport_Temp temp12 where temp12.RecordID = @RecordID) t1
			where ContactID = @ExistingContactID  and AddressTypeID = @BusinessAddressTypeID
			 END
			ELSE
			BEGIN
			INSERT INTO AgentContacts_Addresses (ContactID, StreetAddress,City, State,Zip, AddressTypeID, isPreferred)
			values (@ExistingContactID,
			(select isnull(BusinessAddressOne,' ') + isnull(BusinessAddressTwo,' ') as StreetAddress from ZZ_CMSImport_Temp where RecordID = @RecordID),
			(select BusinessCity from ZZ_CMSImport_Temp where RecordID = @RecordID),
			(select BusinessState from ZZ_CMSImport_Temp where RecordID = @RecordID),
			(select BusinessPostalCode from ZZ_CMSImport_Temp where RecordID = @RecordID),@BusinessAddressTypeID, 1)
			END
		end

		-- Other Address
		if (isnull((select nullif(OtherAddressOne,'') from @CurRecord),'0') <> '0')
			begin
			IF EXISTS (SELECT 1 from AgentContacts_Addresses where ContactID = @ExistingContactID and AddressTypeID = @OtherAddressTypeID)
		BEGIN	
			update dbo.AgentContacts_Addresses set		
			StreetAddress = t1.StreetAddress,
			City = t1.OtherAddressCity,
			State = t1.OtherAddressState,
			Zip = t1.OtherAddressPostalCode			
		FROM	(select isnull(OtherAddressOne,' ') + isnull(OtherAddressTwo,' ') as StreetAddress, OtherAddressCity, OtherAddressState, OtherAddressPostalCode from ZZ_CMSImport_Temp temp12 where temp12.RecordID = @RecordID) t1
			where ContactID = @ExistingContactID  and AddressTypeID = @OtherAddressTypeID
			 END
			ELSE
			BEGIN
			INSERT INTO AgentContacts_Addresses (ContactID, StreetAddress,City, State,Zip, AddressTypeID, isPreferred)
			values (@ExistingContactID,
			(select isnull(OtherAddressOne,' ') + isnull(OtherAddressTwo,' ') as StreetAddress from ZZ_CMSImport_Temp where RecordID = @RecordID),
			(select OtherAddressCity from ZZ_CMSImport_Temp where RecordID = @RecordID),
			(select OtherAddressState from ZZ_CMSImport_Temp where RecordID = @RecordID),
			(select OtherAddressPostalCode from ZZ_CMSImport_Temp where RecordID = @RecordID),@OtherAddressTypeID, 0)
			END
		end


		
		---- Birthday
		print 'Birthday'
		select BirthDay,'1' from @CurRecord
		if (isnull((select nullif(Birthday,'') from @CurRecord),'0') <> '0')
			begin
			IF EXISTS (SELECT 1 from AgentContacts_Dates where ContactID = @ExistingContactID and ContactDateTypeID = @BirthDayTypeID)
		BEGIN	
			update dbo.AgentContacts_Dates set	
			ContactDateDate = t1.Birthday,
			ContactDateNote = ''
		FROM	(select Birthday from ZZ_CMSImport_Temp temp13 where temp13.RecordID = @RecordID) t1
			where ContactID = @ExistingContactID and  ContactDateTypeID = @BirthDayTypeID
			END
			ELSE
			BEGIN
			INSERT INTO AgentContacts_Dates (ContactID, ContactDateDate,ContactDateTypeID,ContactDateNote)
			values (@ExistingContactID, (select Birthday from ZZ_CMSImport_Temp where RecordID = @RecordID),@BirthDayTypeID,'')
			END
		end

		print 'Birthday'

		-- Anniversary
		
		if (isnull((select nullif(Anniversary,'') from @CurRecord),'0') <> '0')
			begin
			IF EXISTS (SELECT 1 from AgentContacts_Dates where ContactID = @ExistingContactID and ContactDateTypeID = @AnniversaryTypeID)
		BEGIN	
			update dbo.AgentContacts_Dates set
			ContactDateTypeID = @AnniversaryTypeID,
			ContactDateDate = t1.Anniversary,
			ContactDateNote = ''
		FROM	(select Anniversary from ZZ_CMSImport_Temp temp14 where temp14.RecordID = @RecordID) t1
			where ContactID = @ExistingContactID and ContactDateTypeID = @AnniversaryTypeID
			END
			ELSE
			BEGIN
			INSERT INTO AgentContacts_Dates (ContactID, ContactDateDate,ContactDateTypeID,ContactDateNote)
			values (@ExistingContactID, (select Anniversary from ZZ_CMSImport_Temp where RecordID = @RecordID), @AnniversaryTypeID,'')
			END
		end
		---- Group Membership
		-- If a group is specified
		
		-- import group (default)
		--	update dbo.AgentContacts_GroupMembership set
		--	GroupID = @GroupID, 
		--	InsertDate =  getdate(), 
		--	HPrimeID = t1.HPrimeID, 
		--	isActive = 1
		--FROM	(select HPrimeID  from ZZ_CMSImport_Temp temp15 where temp15.RecordID = @RecordID) t1
		--	where ContactID = @ExistingContactID

		if (isnull((select nullif(GroupID,'') from @CurRecord),0) <> 0)
			begin
			update dbo.AgentContacts_GroupMembership set
			GroupID = t1.GroupID, 
			InsertDate =  getdate(), 
			HPrimeID = t1.HPrimeID, 
			isActive = 1
			FROM (select GroupID, HPrimeID from ZZ_CMSImport_Temp temp16 where temp16.RecordID = @RecordID) t1
			where ContactID = @ExistingContactID
		end

		update ZZ_CMSImport_Temp set Imported = 1 where RecordID = @RecordID
	

end
else
begin

    --select * from ZZ_CMSImport_Temp

    print 'go for add'
	-- Create the Main Record
	insert into dbo.AgentContacts_Main
	(HPAgentID, ContactTitle, ContactFirstName, ContactMiddleName, ContactLastName, ContactSuffix, ContactInsertDate, ContactDescription, ContactGUID, Active)
	(select 
		gs.HPrimeID, gs.Title, gs.FirstName, gs.MiddleName, gs.LastName, gs.Suffix, getdate(), Notes, newid(), 1
	from ZZ_CMSImport_Temp gs where gs.RecordID = @RecordID)
	-- Get New Contact ID
	declare @newcontactid int
	set @newcontactid = (select top 1 ContactID from AgentContacts_Main order by ContactID desc)
	-- 
	-- Add Email Addresses
	-- 
	-- Email One
	if (isnull((select  nullif(EmailAddress,'') from @CurRecord),'0') <> '0')
	begin
		insert into dbo.AgentContacts_EmailAddresses
		(ContactID, ContactEmailAddress,EmailTypeID, isPreferred)
		select @newcontactid, EmailAddress,@EmailAddressTypeID, 1 from ZZ_CMSImport_Temp gs where gs.RecordID = @RecordID
	end
	-- Email Two
	if (isnull((select nullif(Email2Address,'') from @CurRecord),'0') <> '0')
	begin
		insert into dbo.AgentContacts_EmailAddresses
		(ContactID, ContactEmailAddress,EmailTypeID, isPreferred)
		select @newcontactid, Email2Address,@EmailAddress2TypeID, 0 from ZZ_CMSImport_Temp gs where gs.RecordID = @RecordID
	end
	
	-- Email Three
	if (isnull((select nullif(Email3Address,'') from @CurRecord),'0') <> '0')
	begin
		insert into dbo.AgentContacts_EmailAddresses
		(ContactID, ContactEmailAddress,EmailTypeID, isPreferred)
		select @newcontactid, Email3Address,@EmailAddress3TypeID, 0 from ZZ_CMSImport_Temp gs where gs.RecordID = @RecordID
	end


		-- Email Four
	if (isnull((select nullif(Email4Address,'') from @CurRecord),'0') <> '0')
	begin
		insert into dbo.AgentContacts_EmailAddresses
		(ContactID, ContactEmailAddress,EmailTypeID, isPreferred)
		select @newcontactid, Email4Address,@EmailAddress4TypeID, 0 from ZZ_CMSImport_Temp gs where gs.RecordID = @RecordID
	end

		-- Email Five
	if (isnull((select nullif(Email5Address,'') from @CurRecord),'0') <> '0')
	begin
		insert into dbo.AgentContacts_EmailAddresses
		(ContactID, ContactEmailAddress,EmailTypeID, isPreferred)
		select @newcontactid, Email5Address,@EmailAddress5TypeID, 0 from ZZ_CMSImport_Temp gs where gs.RecordID = @RecordID
	end



	-- 
	-- Add Phone Numbers
	-- 
	-- PrimaryPhone
	--if (isnull((select PrimaryPhone from ZZ_CMSImport_Temp gs where gs.RecordID = @RecordID),'0') <> '0')
	--	begin
	--	insert into dbo.AgentContacts_PhoneNumbers
	--	(ContactID, PhoneTypeID, ContactPhoneNumber, IsPreferred)
	--	select @newcontactid, 1, PrimaryPhone, 1 from ZZ_CMSImport_GmailScrubbed gs where gs.RecordID = @RecordID
	--end
	-- HomePhone
	if (isnull((select nullif(HomePhone,'') from @CurRecord),'0') <> '0')
		begin
		insert into dbo.AgentContacts_PhoneNumbers
		(ContactID, PhoneTypeID, ContactPhoneNumber, IsPreferred)
		select @newcontactid, 2, HomePhone, 0 from ZZ_CMSImport_Temp gs where gs.RecordID = @RecordID
	end
	-- HomePhoneTwo
	if (isnull((select nullif(HomePhoneTwo,'') from @CurRecord),'0') <> '0')
		begin
		insert into dbo.AgentContacts_PhoneNumbers
		(ContactID, PhoneTypeID, ContactPhoneNumber, IsPreferred)
		select @newcontactid, 2, HomePhoneTwo, 0 from ZZ_CMSImport_Temp gs where gs.RecordID = @RecordID
	end

	-- Other Address Phone
	if (isnull((select nullif(OtherAddressPhone,'') from @CurRecord),'0') <> '0')
		begin
		insert into dbo.AgentContacts_PhoneNumbers
		(ContactID, PhoneTypeID, ContactPhoneNumber, IsPreferred)
		select @newcontactid, @OtherAddressPhoneTypeID, OtherAddressPhone, 0 from ZZ_CMSImport_Temp gs where gs.RecordID = @RecordID
	end
	-- Other Address Phone Two
	if (isnull((select nullif(OtherAddressPhoneTwo,'') from @CurRecord),'0') <> '0')
		begin
		insert into dbo.AgentContacts_PhoneNumbers
		(ContactID, PhoneTypeID, ContactPhoneNumber, IsPreferred)
		select @newcontactid, @OtherAddressPhone2TypeID, OtherAddressPhoneTwo, 0 from ZZ_CMSImport_Temp gs where gs.RecordID = @RecordID
	end



	-- Mobile Phone
	if (isnull((select nullif(MobilePhone,'') from @CurRecord),'0') <> '0')
		begin
		insert into dbo.AgentContacts_PhoneNumbers
		(ContactID, PhoneTypeID, ContactPhoneNumber, IsPreferred)
		select @newcontactid, @MobilePhoneTypeID, MobilePhone, 1 from ZZ_CMSImport_Temp gs where gs.RecordID = @RecordID
	end

	-- Mobile Phone 2
	if (isnull((select nullif(MobilePhoneTwo,'') from @CurRecord),'0') <> '0')
		begin
		insert into dbo.AgentContacts_PhoneNumbers
		(ContactID, PhoneTypeID, ContactPhoneNumber, IsPreferred)
		select @newcontactid, @MobilePhone2TypeID, MobilePhoneTwo, 1 from ZZ_CMSImport_Temp gs where gs.RecordID = @RecordID
	end


	-- Other Phone 
	if (isnull((select nullif(OtherPhone,'') from @CurRecord),'0') <> '0')
		begin
		insert into dbo.AgentContacts_PhoneNumbers
		(ContactID, PhoneTypeID, ContactPhoneNumber, IsPreferred)
		select @newcontactid, @OtherPhoneTypeID, OtherPhone, 1 from ZZ_CMSImport_Temp gs where gs.RecordID = @RecordID
	end
	

	-- Business Phone
	if (isnull((select nullif(BusinessPhone,'') from @CurRecord),'0') <> '0')
		begin
		insert into dbo.AgentContacts_PhoneNumbers
		(ContactID, PhoneTypeID, ContactPhoneNumber, IsPreferred)
		select @newcontactid, @BusinessPhoneTypeID, BusinessPhone, 0 from ZZ_CMSImport_Temp gs where gs.RecordID = @RecordID
	end
	-- BusinessPhoneTwo
	if (isnull((select nullif(BusinessPhoneTwo,'') from @CurRecord),'0') <> '0')
		begin
		insert into dbo.AgentContacts_PhoneNumbers
		(ContactID, PhoneTypeID, ContactPhoneNumber, IsPreferred)
		select @newcontactid, @BusinessPhone2TypeID, BusinessPhoneTwo, 0 from ZZ_CMSImport_Temp gs where gs.RecordID = @RecordID
	end
	-- Home Fax
	if (isnull((select nullif(HomeFax,'') from @CurRecord),'0') <> '0')
		begin
		insert into dbo.AgentContacts_PhoneNumbers
		(ContactID, PhoneTypeID, ContactPhoneNumber, IsPreferred)
		select @newcontactid, @HomeFaxTypeID, HomeFax, 0 from ZZ_CMSImport_Temp gs where gs.RecordID = @RecordID
	end
	-- Business Fax
	if (isnull((select nullif(HomePhoneTwo,'') from @CurRecord),'0') <> '0')
		begin
		insert into dbo.AgentContacts_PhoneNumbers
		(ContactID, PhoneTypeID, ContactPhoneNumber, IsPreferred)
		select @newcontactid, @HomePhone2TypeID, BusinessFax, 0 from ZZ_CMSImport_Temp gs where gs.RecordID = @RecordID
	end

	 -- Other Address Phone
	if (isnull((select nullif(OtherAddressPhone,'') from @CurRecord),'0') <> '0')
		begin
		insert into dbo.AgentContacts_PhoneNumbers
		(ContactID, PhoneTypeID, ContactPhoneNumber, IsPreferred)
		select @newcontactid, @OtherAddressPhoneTypeID, OtherAddressPhone, 0 from ZZ_CMSImport_Temp gs where gs.RecordID = @RecordID
	end
	
	 -- Other Address Phone Two
	if (isnull((select nullif(OtherAddressPhoneTwo,'') from @CurRecord),'0') <> '0')
		begin
		insert into dbo.AgentContacts_PhoneNumbers
		(ContactID, PhoneTypeID, ContactPhoneNumber, IsPreferred)
		select @newcontactid, @OtherAddressPhone2TypeID, OtherAddressPhoneTwo, 0 from ZZ_CMSImport_Temp gs where gs.RecordID = @RecordID
	end


	-- Other Address Fax
	if (isnull((select nullif(OtherAddressFax,'') from @CurRecord),'0') <> '0')
		begin
		insert into dbo.AgentContacts_PhoneNumbers
		(ContactID, PhoneTypeID, ContactPhoneNumber, IsPreferred)
		select @newcontactid, @OtherAddressFaxTypeID, OtherAddressFax, 0 from ZZ_CMSImport_Temp gs where gs.RecordID = @RecordID
	end

	
	-- 
	-- Add Addresses 
	-- 
	-- Home Address
	if (isnull((select nullif(HomeAddressOne,'') from @CurRecord),'0') <> '0')
		begin
		insert into dbo.AgentContacts_Addresses
		(ContactID, StreetAddress, City, State, Zip,  isPreferred, AddressTypeID)
		select @newcontactid, isnull(HomeAddressOne,' ') + isnull(HomeAddressTwo,' '), HomeCity, HomeState, HomeZip, 1, @HomeAddressTypeID from ZZ_CMSImport_Temp gs where gs.RecordID = @RecordID
	end
	-- Business Address
	if (isnull((select nullif(BusinessAddressOne,'') from @CurRecord),'0') <> '0')
		begin
		insert into dbo.AgentContacts_Addresses
		(ContactID, StreetAddress, City, State, Zip, isPreferred, AddressTypeID)
		select @newcontactid, isnull(BusinessAddressOne,' ') + isnull(BusinessAddressTwo,' '), BusinessCity, BusinessState, BusinessPostalCode, 0, @BusinessAddressTypeID from ZZ_CMSImport_Temp gs where gs.RecordID = @RecordID
	end

	-- Other Address
	if (isnull((select nullif(OtherAddressOne,'') from @CurRecord),'0') <> '0')
		begin
		insert into dbo.AgentContacts_Addresses
		(ContactID, StreetAddress, City, State, Zip, isPreferred, AddressTypeID)
		select @newcontactid, isnull(OtherAddressOne,' ') + isnull(OtherAddressTwo,' '), OtherAddressCity, OtherAddressState, OtherAddressPostalCode, 0, @OtherAddressTypeID from ZZ_CMSImport_Temp gs where gs.RecordID = @RecordID
	end

	
	---- Birthday

	if (isnull((select nullif(Birthday,'') from @CurRecord),'0') <> '0')
		begin
		insert into dbo.AgentContacts_Dates
		(ContactID, ContactDateTypeID, ContactDateDate, ContactDateNote )
		select @newcontactid,  @BirthDayTypeID, Birthday, '' from ZZ_CMSImport_Temp gs where gs.RecordID = @RecordID
	end
	-- Anniversary
	
	if (isnull((select nullif(Anniversary,'') from @CurRecord),'0') <> '0')
		begin
		insert into dbo.AgentContacts_Dates
		(ContactID, ContactDateTypeID, ContactDateDate, ContactDateNote )
		select @newcontactid,  @AnniversaryTypeID, Anniversary, '' from ZZ_CMSImport_Temp gs where gs.RecordID = @RecordID
	end
	---- Group Membership
	-- If a group is specified
	
	-- import group (default)
		--insert into dbo.AgentContacts_GroupMembership
		--( ContactID, GroupID, InsertDate, HPrimeID, isActive)
		--select @newcontactid, @GroupID, getdate(), HPrimeID, 1 from ZZ_CMSImport_Temp gs where gs.RecordID = @RecordID

	if (isnull((select nullif(GroupID,'') from @CurRecord),0) <> 0)
		begin
		insert into dbo.AgentContacts_GroupMembership
		( ContactID, GroupID, InsertDate, HPrimeID, isActive)
		select @newcontactid, GroupID, getdate(), HPrimeID, 1 from ZZ_CMSImport_Temp gs where gs.RecordID = @RecordID
	end

	update ZZ_CMSImport_Temp set Imported = 1 where RecordID = @RecordID
end
END