USE [ContactsImport]
GO
/****** Object:  StoredProcedure [dbo].[CRM_ImportContactsFromFile]    Script Date: 11/5/2016 9:22:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [dbo].[CRM_ImportContactsFromFile] 
(
  @XMLString NTEXT
  , @HPrimeID int
)
AS
BEGIN

 DECLARE @hDoc INT -- Handle for Xml document
 EXEC sp_xml_preparedocument @hDoc OUTPUT, @XMLString 
  
  INSERT INTO ZZ_CMSImport_Temp
			(
             FirstName
			,MiddleName
            ,LastName
            ,SpouseName
			,Title
			,Suffix
            ,Initials
			,WebPage
			,Birthday
			,Anniversary
			,Notes
			,EmailAddress
			,Email2Address
			,Email3Address			
			,HomePhone
			,HomePhoneTwo
			,HomeFax
			,MobilePhone
			,HomeAddressOne
			,HomeAddressTwo
			,HomeCity
			,HomeState
			,HomeCountry
			,HomeZip
			,BusinessPhone
			,BusinessPhoneTwo
			,BusinessFax
			,BusinessAddressOne
			,BusinessAddressTwo
			,BusinessCity
			,BusinessState
			,BusinessPostalCode
			,BusinessCounty

			,OtherAddressOne
			,OtherAddressTwo
			,OtherAddressCity
			,OtherAddressState
			,OtherAddressCountry
			,OtherAddressPostalCode
			,OtherAddressPhone
			,OtherAddressPhoneTwo
			,OtherAddressFax
			,MobilePhoneTwo
			,OtherPhone
			,Email4Address
			,Email5Address
			,Spouse
			,Children
			,Company
			,Gender
			,Profession


			,Categories
			,HPrimeID
			,GroupId
			)
			SELECT 
             FirstName
			,MiddleName
            ,LastName
            ,SpouseName
			,Title
			,Suffix
            ,Initials
			,WebPage
			,Birthday
			,Anniversary
			,Notes
			,EmailAddress
			,Email2Address
			,Email3Address			
			,HomePhone
			,HomePhoneTwo
			,HomeFax
			,MobilePhone
			,HomeAddressOne
			,HomeAddressTwo
			,HomeCity
			,HomeState
			,HomeCountry
			,HomeZip
			,BusinessPhone
			,BusinessPhoneTwo
			,BusinessFax
			,BusinessAddressOne
			,BusinessAddressTwo
			,BusinessCity
			,BusinessState
			,BusinessPostalCode
			,BusinessCounty

			,OtherAddressOne
			,OtherAddressTwo
			,OtherAddressCity
			,OtherAddressState
			,OtherAddressCountry
			,OtherAddressZip
			,OtherAddressPhone
			,OtherAddressPhoneTwo
			,OtherAddressFax
			,MobilePhoneTwo
			,OtherPhone
			,Email4Address
			,Email5Address
			,Spouse
			,Children
			,Company
			,Gender
			,Profession

			,Categories
			,HPrimeID
			,GroupId
            
			FROM OPENXML(@hDoc,'NewDataSet/Table1',2)
			WITH
			(
			  FirstName nvarchar(500)
			 ,MiddleName   nvarchar(500)
            ,LastName  nvarchar(500)            
			,SpouseName  nvarchar(500)
			,Title nvarchar(500)
			,Suffix  nvarchar(500)
            ,Initials  nvarchar(500)
			,WebPage  nvarchar(500)
			,Birthday nvarchar(500)
			,Anniversary nvarchar(500)
			,Notes nvarchar(max)
			,EmailAddress nvarchar(500)
			,Email2Address nvarchar(500)
			,Email3Address nvarchar(500)			
			,HomePhone nvarchar(500)
			,HomePhoneTwo nvarchar(500)
			,HomeFax nvarchar(500)
			,MobilePhone nvarchar(500)
			,HomeAddressOne nvarchar(500)
			,HomeAddressTwo nvarchar(500)
			,HomeCity nvarchar(500)
			,HomeState nvarchar(500)
			,HomeCountry nvarchar(500)
			,HomeZip nvarchar(500)
			,BusinessPhone nvarchar(500)
			,BusinessPhoneTwo nvarchar(500)
			,BusinessFax nvarchar(500)
			,BusinessAddressOne nvarchar(500)
			,BusinessAddressTwo nvarchar(500)
			,BusinessCity nvarchar(500)
			,BusinessState nvarchar(500)
			,BusinessPostalCode nvarchar(500)
			,BusinessCounty nvarchar(500)

			,OtherAddressOne nvarchar(500)
			,OtherAddressTwo nvarchar(500)
			,OtherAddressCity nvarchar(500)
			,OtherAddressState nvarchar(500)
			,OtherAddressCountry nvarchar(500)
			,OtherAddressZip nvarchar(500)
			,OtherAddressPhone nvarchar(500)
			,OtherAddressPhoneTwo nvarchar(500)
			,OtherAddressFax nvarchar(500)
			,MobilePhoneTwo nvarchar(500)
			,OtherPhone nvarchar(500)
			,Email4Address nvarchar(500)
			,Email5Address nvarchar(500)
			,Spouse nvarchar(500)
			,Children nvarchar(500)
			,Company nvarchar(500)
			,Gender nvarchar(500)
			,Profession nvarchar(500)


			,Categories nvarchar(500)
			,HPrimeID nvarchar(500)
			,GroupId nvarchar(500)
             )


		select * from ZZ_CMSImport_Temp


			 --insert into AgentContacts_Groups
			 --(AgentContactGroupName, HPrimeID, CreatedDate, isActive)
			 --values
			 --('Contact Import ' + convert(nvarchar(20), getdate(), 101), @HPrimeID, getdate(), 1)
			 
			 declare @importgroupID int = (Select top 1 AgentContactGroupID from AgentContacts_Groups ag order by ag.AgentContactGroupID desc)
			 declare @RecordID int

			declare cur CURSOR LOCAL for
				select recordID from ZZ_CMSImport_Temp where isnull(Imported,0) = 0

			open cur

			fetch next from cur into @RecordID

			while @@FETCH_STATUS = 0 BEGIN
				print '------'
				print @RecordID
				print '------'
				--execute your sproc on each row
				exec CRM_ProcessimportAction @RecordID, @importgroupID
				print @RecordID
				fetch next from cur into @RecordID
	
			END

			close cur
			deallocate cur
   
END