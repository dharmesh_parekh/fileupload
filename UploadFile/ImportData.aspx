﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImportData.aspx.cs" Inherits="FileUpload.ImportData" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Data Upload</title>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.4.0/jquery.min.js"></script>
    <style type="text/css">
        .button {
            background: #ececec;
            background: linear-gradient(#f4f4f4, #ececec);
            background: -webkit-linear-gradient(#f4f4f4,#ececec);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#f4f4f4',endColorstr='#ececec',GradientType=0);
            -webkit-box-shadow: 1px 1px 2px #aaa;
            box-shadow: 1px 1px 2px #aaa;
            border: 1px solid #d4d4d4;
            border-radius: 0.2em 0.2em 0.2em 0.2em;
            color: #333;
            cursor: pointer;
            display: inline-block;
            font: 11px sans-serif;
            outline: medium none;
            overflow: visible;
            padding: 4px 5px;
            position: relative;
            text-decoration: none;
            text-shadow: 0 1px 0 rgba(255,255,255,.3);
            white-space: nowrap;
        }

            .button:hover, .button:active, .button:active {
                cursor: default;
                background: #3c8dde;
                background: linear-gradient(#599bdc,#3072b3);
                background: -webkit-linear-gradient(#599bdc,#3072b3);
                filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#599bdc',endColorstr='#3072b3',GradientType=0);
                border-color: #3072b3 #3072b3 #2a65a0;
                color: #fff;
                text-decoration: none;
                text-shadow: 0 -1px 0 rgba(0,0,0,.7);
            }

        .progress {
            height: 12px;
            width: 0;
        }

        .porgesscontainer {
            width: 210px;
            height: 12px;
            border: 1px solid rgb(31,194,69);
            display: none;
            vertical-align: bottom;
        }

        .gradient {
            background: rgb(31,194,69);
            background: linear-gradient(bottom, rgb(112,224,87) 22%, rgb(31,194,69) 56%, rgb(156,247,162) 84%);
            background: -o-linear-gradient(bottom, rgb(112,224,87) 22%, rgb(31,194,69) 56%, rgb(156,247,162) 84%);
            background: -moz-linear-gradient(bottom, rgb(112,224,87) 22%, rgb(31,194,69) 56%, rgb(156,247,162) 84%);
            background: -webkit-linear-gradient(bottom, rgb(112,224,87) 22%, rgb(31,194,69) 56%, rgb(156,247,162) 84%);
            background: -ms-linear-gradient(bottom, rgb(112,224,87) 22%, rgb(31,194,69) 56%, rgb(156,247,162) 84%);
            background: -webkit-gradient(linear,left bottom,left top,color-stop(0.22, rgb(112,224,87)),color-stop(0.56, rgb(31,194,69)),color-stop(0.84, rgb(156,247,162)));
        }

        .odiv {
            background: #476ea2;
            width: 200px;
            height: 150px;
            margin: auto;
            padding: 20px;
            color: #fff;
            text-align: center;
        }

        .odivc {
            height: 100%;
            margin: auto;
            border: 2px dashed #fff;
        }

        .rounded {
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
        }

        .btn {
            cursor: default;
            display: inline;
            -moz-border-radius: 2px;
            -webkit-border-radius: 2px;
            border-radius: 2px;
            background: #476ea2;
            color: #fff;
            font-size: 17px;
            padding: 10px 30px;
            -webkit-box-shadow: 1px 1px 2px #aaa;
            box-shadow: 1px 1px 2px #aaa;
            -webkit-transition: background .5s;
            -moz-transition: background .5s;
            -o-transition: background .5s;
            transition: background .5s;
        }

            .btn:hover {
                background: #476ea2;
                text-decoration: none;
            }
/****************** ItechSoft ***********************/
/****************** 15/10/2015  ***********************/

        .modal {
            position: fixed;
            z-index: 999;
            height: 100%;
            width: 100%;
            top: 0;
            background-color: Black;
            filter: alpha(opacity=60);
            opacity: 0.6;
            -moz-opacity: 0.8;
        }

        .center {
            z-index: 1000;
            position: fixed;
            top: 50%;
            left: 50%;
            margin-top: -50px;
            margin-left: -100px;
            background-color: White;
            border-radius: 10px;
            filter: alpha(opacity=100);
            opacity: 1;
            -moz-opacity: 1;
        }

        .center img {
                height: 128px;
                width: 128px;
            }

        
.waitMask {
    z-index: 10099;
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: #002B49;
}

.pleaseWait {
    z-index: 10100;
    display: flex;
    justify-content: center;
    align-items: center;
    width: 600px;
    margin: 20% auto;
    text-align: center;
}

    .pleaseWait p {
        font-size: 2em;
        color: #ffffff;
    }



/****************** ItechSoft ***********************/
/****************** 15/10/2015  ***********************/


    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div style="height: 30px"></div>
        <h3><a href="ImportData.aspx" style="text-decoration: none;">Upload Data </a>| <a href="SearchData.aspx" style="text-decoration: none;">View Data</a></h3>

        <hr style="border-color: #ccc; border-top: 0" />

        <div id="divTransactionInfo" class="odiv rounded">
            <div class="odivc rounded">
                <%--Does the file you are uploading contain transaction information?--%>
                <br />
                <br />
                <div class="btn" style="width: 50px; background: #476ea2; color: #f39c11"></div>
                <div class="btn" style="width: 50px; background: #476ea2"></div>
            </div>
        </div>



        <div id="divFileUploader" class="odiv rounded" style="display: none">
            <div class="odivc rounded">
                <div id="btnBrowse" style="height: 100%; padding: 10px" ondragover="dragover(event)" ondrop="drop(event)">
                    Drag and drop your file here or click to find the files on your computer
                    <progress id="progress" min="22" max="100" value="0"></progress>
                </div>
            </div>
        </div>

        <div id="divFileHeader" class="odiv rounded" style="display: none">
            <div class="odivc rounded">
                Does the file you uploaded contain column headers?
                <asp:RadioButtonList ID="rbtYesNo" runat="server" RepeatDirection="Horizontal" Width="15%">
                    <asp:ListItem Value="Yes" Selected="True"> Yes</asp:ListItem>
                    <asp:ListItem Value="No"> No</asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>

        <asp:ScriptManager ID="scrMgr" runat="server" />
        <asp:UpdatePanel ID="updPnl" runat="server">
            <ContentTemplate>
                <asp:HiddenField runat="server" ID="hfFilePath" />
                <br />
                <div style="text-align: center">
                    <asp:Button runat="server" ID="btnMapFields" Text="Map Fields" OnClick="btnMapFields_Click" Style="width: 100px; height: 50px; display: none;" />
                </div>
                <div style="width: 750px; margin: auto; color: #fff; padding: 10px">
                    <%--                                       
                                      ModifiedBy ItechSoft 
                                       ModifiedOn 15/10/2016 
                    --%>
                    <asp:GridView ID="grdField" AutoGenerateColumns="false" OnRowDataBound="grdField_RowDataBound" runat="server" BorderColor="#ffffff" HeaderStyle-ForeColor="White"
                        BorderWidth="0" HeaderStyle-Height="32px" BackColor="#476ea2" Width="750px" ForeColor="#FFFFFF"
                        CellSpacing="0" CellPadding="5" Font-Bold="true" Font-Size="Small">
                        <Columns>
                            <asp:BoundField HeaderText="Column Name" DataField="FieldName" ItemStyle-Width="35%" />
                            <asp:BoundField HeaderText="Sample Data" DataField="RecordName" ItemStyle-Width="35%" />
                            <asp:TemplateField HeaderText="What is this?" ItemStyle-Width="65%">
                                <ItemTemplate>
                                    <%--                                       
                                      ModifiedBy ItechSoft 
                                       ModifiedOn 15/10/2016 
                                    --%>
                                    <asp:DropDownList AutoPostBack="true" OnSelectedIndexChanged="ddlDbFields_SelectedIndexChanged" ID="ddlDbFields" runat="server" Width="100%" OnPreRender="veriProgressStatLoad" BackColor="#476ea2" ForeColor="#fff">
                                        <%--  <asp:ListItem Text="Select" Value="" style="color: Green; font-weight: bold" />
                                        <asp:ListItem Value="Title" Text="Title" />
                                        <asp:ListItem Value="FirstName" Text="FirstName" />
                                        <asp:ListItem Value="MiddleName" Text="MiddleName" />
                                        <asp:ListItem Value="LastName" Text="Last Name" />
                                        <asp:ListItem Value="FullName" Text="Full Name" />
                                        <asp:ListItem Value="SpouseName" Text="Spouse Name" />                                     
                                        <asp:ListItem Value="Suffix" Text="Suffix" />
                                        <asp:ListItem Value="Initials" Text="Initials" />
                                        <asp:ListItem Value="WebPage" Text="WebPage" />
                                        <asp:ListItem Value="Birthday" Text="Birthday" />
                                        <asp:ListItem Value="Anniversary" Text="Anniversary" />
                                        <asp:ListItem Value="Notes" Text="Notes" />
                                        <asp:ListItem Value="EmailAddress" Text="EmailAddress" />
                                        <asp:ListItem Value="Email2Address" Text="Email Address 2" />
                                        <asp:ListItem Value="Email3Address" Text="Email Address 3" />
                                        <asp:ListItem Value="HomePhone" Text="Home Phone" />
                                        <asp:ListItem Value="HomePhoneTwo" Text="Home Phone 2" />
                                        <asp:ListItem Value="HomeFax" Text="HomeFax" />
                                        <asp:ListItem Value="MobilePhone" Text="Mobile Phone" />
                                        <asp:ListItem Value="HomeAddressOne" Text="Home Address 1" />
                                        <asp:ListItem Value="HomeAddressTwo" Text="Home Address 2" />
                                        <asp:ListItem Value="HomeCity" Text="Home City" />
                                        <asp:ListItem Value="HomeState" Text="Home State" />
                                        <asp:ListItem Value="HomeZip" Text="Home Zip" />
                                        <asp:ListItem Value="BusinessPhone" Text="Business Phone" />
                                        <asp:ListItem Value="BusinessPhoneTwo" Text="Business Phone 2" />
                                        <asp:ListItem Value="BusinessFax" Text="Business Fax" />
                                        <asp:ListItem Value="BusinessAddressOne" Text="Business Address 1" />
                                        <asp:ListItem Value="BusinessAddressTwo" Text="Business Address 2" />
                                        <asp:ListItem Value="BusinessCity" Text="Business City" />
                                        <asp:ListItem Value="BusinessState" Text="Business State" />
                                        <asp:ListItem Value="BusinessPostalCode" Text="Business Zip" />
                                        <asp:ListItem Value="BusinessCounty" Text="Business County" />
                                        <asp:ListItem Value="Categories" Text="Categories" />
                                        <asp:ListItem Value="" Text="IGNORE" />--%>
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle BackColor="#476ea2" Font-Size="10pt" ForeColor="Black" />
                    </asp:GridView>
                </div>
                <div style="text-align: center">
                    <asp:Button ID="btnImport" runat="server" Text="Import" class="button" OnClick="btnImport_Click" />

                    <asp:HyperLink runat="server" ID="btnShowSearchResult" Text="Show Search Result" Visible="false" />
                </div>
                <asp:Label runat="server" ID="lblMessage" />
                <asp:HiddenField runat="server" ID="hfUploadID" />
                <asp:HiddenField runat="server" ID="hfFileID" />
            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:UpdateProgress runat="server" AssociatedUpdatePanelID="updPnl" DisplayAfter="50">
            <ProgressTemplate>             
                 <%--                                       
                                      ModifiedBy ItechSoft 
                                       ModifiedOn 15/10/2016 
                  --%>
               <div class="waitMask" onclick="return DivClick();">
    <div class="pleaseWait">
        <p>
            <%--Uploading File,--%> Please Wait <i class="fa fa-spinner fa-spin"></i>
        </p>
    </div>
</div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <input id="fupload" type="file" onchange="startRequest()" style="display: none" />
        <br />

        <hr style="border-color: #ccc; border-top: 0" />
        <br />
        <br />
        <br />
        <div id="progcontainer" class="porgesscontainer">
            <div id="progressbar" class="progress gradient"></div>
        </div>
        <br />
        <div id="divFileInfo"></div>
        <div id="divSteps"></div>

    </form>
    <script type="text/javascript">
        $(".btn").click(function () {
            $(this).closest(".odiv").hide();
            $(this).closest(".odiv").next().show();
        });

        $("#btnUploadBack").click(function () {
            if ($(".odiv").not(":hidden").prev().children().size() > 0) {
                $(".odiv").not(":hidden").hide().prev().show();
            }
        });

        function dragover(evt) { evt.preventDefault(); }

        function drop(evt) {
            evt.preventDefault();
            var formData = new FormData();
            var files = evt.dataTransfer.files;

            formData.append('file', files[0]);



            var xhr = new XMLHttpRequest();

            xhr.open('POST', '?f=1');
            xhr.upload.onprogress = function (event) {
                if (event.lengthComputable) {
                    var complete = (event.loaded / event.total * 100 | 0);
                    progress.value = progress.innerHTML = complete;
                }
            };

            xhr.onload = function () {
                if (xhr.status === 200) {
                    progress.value = progress.innerHTML = 100;
                    currentStep(xhr.responseText);
                }
                else { }
            };

            xhr.send(formData);

        }



        $(function () {

            $('#divFileUploader').removeAttr('style');
            $('#divTransactionInfo').hide();

        })

        $('#btnBrowse').click(function () { $('input:file').click(); });
        var xmlHttp;
        function createXMLHttpRequest() {
            if (window.ActiveXObject) { xmlHttp = new ActiveXObject('Microsoft.XMLHTTP'); }
            else if (window.XMLHttpRequest) { xmlHttp = new XMLHttpRequest(); }
        }
        function updateProgress(evt) {
            if (evt.lengthComputable) {
                $('#divFileInfo').text(Math.floor(evt.loaded / evt.total * 100) + '%');
                $('#progressbar').width(Math.floor((evt.loaded / evt.total * 100) * 2.1) + 'px');
            }
            else { }
        }
        function transferComplete(evt) { $('#divFileInfo').text(100 + '% completed.'); $('#progressbar').width(210 + 'px'); }
        function transferFailed(evt) { alert('An error occurred while transferring the file.') }
        function transferCanceled(evt) { alert('The transfer has been canceled by the user.'); }
        function startRequest() {
            if (FileExtensionValidation()) {
                $('#divFileInfo').removeAttr('style');
                var fd = new FormData();
                fd.append('userfile', fupload.files[0]);
                var fileList = document.getElementById('fupload');
                createXMLHttpRequest();
                xmlHttp.upload.addEventListener('progress', updateProgress, false);
                xmlHttp.upload.addEventListener('load', transferComplete, false);
                xmlHttp.upload.addEventListener('error', transferFailed, false);
                xmlHttp.upload.addEventListener('abort', transferCanceled, false);
                xmlHttp.onreadystatechange = handleStateChange; xmlHttp.open('POST', '?f=1', true);
                $('#progcontainer').show(); xmlHttp.send(fd);
            }
            else { }
        }
        function handleStateChange() {
            if (xmlHttp.readyState == 4) {
                if (xmlHttp.status == 200) {
                    $('#divSteps').html('File uploaded.');
                    currentStep(xmlHttp.responseText)

                    $('#progcontainer').hide();
                }
            }
        }
        function currentStep(d) {
            var arr = eval(d);
            for (var f in arr) {
                $("#hfFilePath").val(arr[f].FileName);
                $("#btnMapFields").trigger('click');
                $(".odiv").not(":hidden").hide();
            }

        }
        function mapFields(c) {
            prmData = {};
            prmData.filename = c
            $.ajax({
                type: "POST", async: true, url: "ImportData.aspx/mapField", data: JSON.stringify(prmData), contentType: "application/json; charset=utf-8", dataType: "json", success: function onS(res) {

                    $('#divSteps').append('<br />Maped field names ' + c)
                }, error: function onF(err) { }
            });

        }

        /*FileType Validation*/
        function FileExtensionValidation() {
            var fileUpload = $('input[id$=fupload]').val();
            if (fileUpload == "") {
                $('#divFileInfo').text("Please select file to upload.");
                return false;
            }
            else {
                var validFilesTypes = ["xls", "xlsx", "csv"];
                var file = document.getElementById('fupload');
                var label = $('#divFileInfo');
                var path = file.value;
                var ext = path.substring(path.lastIndexOf(".") + 1, path.length).toLowerCase();
                var isValidFile = false;
                for (var i = 0; i < validFilesTypes.length; i++) {
                    if (ext == validFilesTypes[i]) {
                        isValidFile = true;
                        break;
                    }
                }
                if (!isValidFile) {
                    $(label).css('color', "red");
                    $(label).text("Invalid File. Please upload a File with" + " extension:\n\n" + validFilesTypes.join(", "));
                }
                return isValidFile;
            }
        }
    </script>
</body>
</html>
