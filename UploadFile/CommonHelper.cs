﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace FileUpload
{

    //Added By ItechSoft
    // Added on 14/10/1016
    public static class CommonHelper
    {
        public static void ReorderAlphabetized(this DropDownList ddl)
        {
            List<ListItem> listCopy = new List<ListItem>();
            foreach (ListItem item in ddl.Items)
                listCopy.Add(item);
            var selectItem = listCopy.Where(x => x.Text != null && x.Text.ToLower() == "select").FirstOrDefault();
            listCopy.Remove(selectItem);
            ddl.Items.Clear();
            foreach (ListItem item in listCopy.OrderBy(item => item.Text))
            {
                ddl.Items.Add(item);
            }
            ddl.Items.Insert(0, selectItem);
        }
        public static string GetDescriptionFromEnumValue(Enum value)
        {
            DescriptionAttribute attribute = value.GetType()
                .GetField(value.ToString())
                .GetCustomAttributes(typeof(DescriptionAttribute), false)
                .SingleOrDefault() as DescriptionAttribute;
            return attribute == null ? value.ToString() : attribute.Description;
        }

    }


}