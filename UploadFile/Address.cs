﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FileUpload
{  
    // Added by ItechSoft
    // Added on 22/10/2016

    public class Address
    {
        public string AddressOne { get; set; }
        public string AddressTwo { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
    }

}