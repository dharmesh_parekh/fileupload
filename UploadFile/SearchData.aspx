﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SearchData.aspx.cs" Inherits="FileUpload.SearchData" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Data Upload</title>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.4.0/jquery.min.js"></script>
    <style type="text/css">
        .button
        {
            background: #ececec;
            background: linear-gradient(#f4f4f4, #ececec);
            background: -webkit-linear-gradient(#f4f4f4,#ececec);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#f4f4f4',endColorstr='#ececec',GradientType=0);
            -webkit-box-shadow: 1px 1px 2px #aaa;
            box-shadow: 1px 1px 2px #aaa;
            border: 1px solid #d4d4d4;
            border-radius: 0.2em 0.2em 0.2em 0.2em;
            color: #333;
            cursor: pointer;
            display: inline-block;
            font: 11px sans-serif;
            outline: medium none;
            overflow: visible;
            padding: 4px 5px;
            position: relative;
            text-decoration: none;
            text-shadow: 0 1px 0 rgba(255,255,255,.3);
            white-space: nowrap;
        }

            .button:hover, .button:active, .button:active
            {
                cursor: default;
                background: #3c8dde;
                background: linear-gradient(#599bdc,#3072b3);
                background: -webkit-linear-gradient(#599bdc,#3072b3);
                filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#599bdc',endColorstr='#3072b3',GradientType=0);
                border-color: #3072b3 #3072b3 #2a65a0;
                color: #fff;
                text-decoration: none;
                text-shadow: 0 -1px 0 rgba(0,0,0,.7);
            }

        .progress
        {
            height: 12px;
            width: 0;
        }

        .porgesscontainer
        {
            width: 210px;
            height: 12px;
            border: 1px solid rgb(31,194,69);
            display: none;
            vertical-align: bottom;
        }

        .gradient
        {
            background: rgb(31,194,69);
            background: linear-gradient(bottom, rgb(112,224,87) 22%, rgb(31,194,69) 56%, rgb(156,247,162) 84%);
            background: -o-linear-gradient(bottom, rgb(112,224,87) 22%, rgb(31,194,69) 56%, rgb(156,247,162) 84%);
            background: -moz-linear-gradient(bottom, rgb(112,224,87) 22%, rgb(31,194,69) 56%, rgb(156,247,162) 84%);
            background: -webkit-linear-gradient(bottom, rgb(112,224,87) 22%, rgb(31,194,69) 56%, rgb(156,247,162) 84%);
            background: -ms-linear-gradient(bottom, rgb(112,224,87) 22%, rgb(31,194,69) 56%, rgb(156,247,162) 84%);
            background: -webkit-gradient(linear,left bottom,left top,color-stop(0.22, rgb(112,224,87)),color-stop(0.56, rgb(31,194,69)),color-stop(0.84, rgb(156,247,162)));
        }

        .odiv
        {
            background: #476ea2;
            width: 200px;
            height: 150px;
            margin: auto;
            padding: 20px;
            color: #fff;
            text-align: center;
        }

        .odivc
        {
            height: 100%;
            margin: auto;
            border: 2px dashed #fff;
        }

        .rounded
        {
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
        }

        .btn
        {
            cursor: default;
            display: inline;
            -moz-border-radius: 2px;
            -webkit-border-radius: 2px;
            border-radius: 2px;
            background: #476ea2;
            color: #fff;
            font-size: 17px;
            padding: 10px 30px;
            -webkit-box-shadow: 1px 1px 2px #aaa;
            box-shadow: 1px 1px 2px #aaa;
            -webkit-transition: background .5s;
            -moz-transition: background .5s;
            -o-transition: background .5s;
            transition: background .5s;
        }

            .btn:hover
            {
                background: #476ea2;
                text-decoration: none;
            }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div style="height: 30px;"></div>
        <h3><a href="ImportData.aspx" style="text-decoration: none;">Upload Data </a>| <a href="SearchData.aspx" style="text-decoration: none;">View Data</a></h3>

        <hr style="border-color: #ccc; border-top: 0" />

        <div style="height: 500px; margin: auto; color: #fff; padding: 0px 40px; overflow: scroll;">
            <asp:GridView ID="grdField" runat="server" BorderColor="#ffffff" HeaderStyle-ForeColor="White"
                BorderWidth="0" HeaderStyle-Height="32px" BackColor="#476ea2" ForeColor="#FFFFFF"
                CellSpacing="0" CellPadding="5" Font-Bold="true" Font-Size="Small" PageSize="20" AllowPaging="True" OnPageIndexChanging="grdField_PageIndexChanging">
                <HeaderStyle BackColor="#476ea2" Font-Size="10pt" ForeColor="Black" />
            </asp:GridView>
        </div>
        <div style="text-align: center">
        </div>

        <br />
        <hr style="border-color: #ccc; border-top: 0" />
        <br />
        <br />
        <br />
        <div id="progcontainer" class="porgesscontainer">
            <div id="progressbar" class="progress gradient"></div>
        </div>
        <br />
        <div id="divFileInfo"></div>
        <div id="divSteps"></div>

    </form>
</body>
</html>
