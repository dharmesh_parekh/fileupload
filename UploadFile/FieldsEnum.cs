﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace FileUpload
{

    //Added By ItechSoft
    // Added on 14/10/1016
    public enum FieldsEnum
    {


        [Description("Title")]
        Title = 1,
        [Description("First Name")]
        FirstName = 2,

        [Description("Middle Name")]
        MiddleName = 3,

        [Description("Last Name")]
        LastName = 4,
        [Description("Full Name")]
        FullName = 5,
        [Description("Spouse Name")]
        SpouseName = 6,
        [Description("Suffix")]
        Suffix = 7,
        [Description("Initials")]
        Initials = 8,
        [Description("WebPage")]
        WebPage = 9,
        [Description("Birthday")]
        Birthday = 10,
        [Description("Anniversary")]
        Anniversary = 11,
        [Description("Notes")]
        Notes = 12,
        [Description("Email Address")]
        EmailAddress = 13,
        [Description("Email Address 2")]
        Email2Address = 14,

        [Description("Email Address 3")]
        Email3Address = 15,

        [Description("Home Phone")]
        HomePhone = 16,
        [Description("Home Phone 2")]
        HomePhoneTwo = 17,
        [Description("Home Fax")]
        HomeFax = 18,
        [Description("Mobile Phone")]
        MobilePhone = 19,
        [Description("Home Address 1")]
        HomeAddressOne = 20,
        [Description("Home Address 2")]
        HomeAddressTwo = 21,
        [Description("Home City")]
        HomeCity = 22,
        [Description("Home State")]
        HomeState = 23,
        [Description("Home Zip")]
        HomeZip = 24,
        [Description("Business Phone")]
        BusinessPhone = 25,
        [Description("Business Phone 2")]
        BusinessPhoneTwo = 26,
        [Description("Business Fax")]
        BusinessFax = 27,
        [Description("Business Address 1")]
        BusinessAddressOne = 28,
        //Modified by ItechSoft
        //Modified on 04/11/2016
        [Description("Business Address 2")]
        BusinessAddressTwo = 29,
        [Description("Business City")]
        BusinessCity = 30,
        [Description("Business State")]
        BusinessState = 31,
        [Description("Business Zip")]
        BusinessZip = 32,
        [Description("Business Country")]
        BusinessCounty = 33,
        //Modified by ItechSoft
        //Modified on 04/11/2016
        //[Description("Categories")]
        //Categories = 34,    
        [Description("Home Full Address")]
        HomeFullAddress = 35,
        [Description("Business Full Address")]
        BusinessFullAddress = 36,
        //[Description("IGNORE")]
        //IGNORE = 37,

        #region ItechSoft 04/11/2016

        [Description("Home Country")]
        HomeCounty = 38,

        [Description("Other Address 1")]
        OtherAddressOne = 39,

        [Description("Other Address 2")]
        OtherAddressTwo = 40,

        [Description("Other Address City")]
        OtherAddressCity = 41,

        [Description("Other Address State")]
        OtherAddressState = 42,

        [Description("Other Address Country")]
        OtherAddressCountry = 43,

        [Description("Other Address Phone")]
        OtherAddressPhone = 44,

        [Description("Other Address Phone 2")]
        OtherAddressPhoneTwo = 45,

        [Description("Other Address Zip")]
        OtherAddressPostalCode = 46,

        [Description("Other Address Fax")]
        OtherAddressFax = 47,

        [Description("Mobile Phone 2")]
        MobilePhoneTwo = 48,

        [Description("Other Phone")]
        OtherPhone = 49,

        [Description("Email Address 4")]
        Email4Address = 50,

        [Description("Email Address 5")]
        Email5Address = 51,

        [Description("Other Full Address")]
        OtherFullAddress = 52,

        [Description("Spouse")]
        Spouse = 53,

        [Description("Children")]
        Children = 54,

        [Description("Company")]
        Company = 55,

        [Description("Gender")]
        Gender = 56,

        [Description("Profession")]
        Profession = 57

        #endregion
    }
}