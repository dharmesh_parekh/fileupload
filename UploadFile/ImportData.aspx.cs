﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Web.Services;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Drawing;
using System.Data.Sql;
using System.Configuration;
using System.Runtime.Serialization.Json;
using System.Text;
using Newtonsoft.Json;
using System.Text.RegularExpressions;

namespace FileUpload
{
    public partial class ImportData : System.Web.UI.Page
    {

        #region ["Parameter Declear"]
        DataTable dtFielsName = new DataTable();
        DataTable dtFielsTrans = new DataTable();
        int countNo = 0;
        int countTransKey = 0;
        string targetFile = string.Empty;
        bool isCheckHeader = false;
        #endregion

        #region["Page Load Event"]
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.Request.Files.Count > 0)
            {
                List<UploadedFiles> uFIles = new List<UploadedFiles>();
                for (int i = 0; i <= Page.Request.Files.Count - 1; i++)
                {
                    string fileExtension = Path.GetExtension(Page.Request.Files[i].FileName).ToUpper();


                    string fullPath = string.Empty;
                    try
                    {
                        if (fileExtension == ".XLSX" || fileExtension == ".XLS" || fileExtension == ".CSV")
                        {

                            string filename = Path.GetFileNameWithoutExtension(Page.Request.Files[i].FileName) + DateTime.Now.Ticks.ToString();
                            string foldername = "Files";
                            fullPath = Server.MapPath("~/" + foldername + @"\" + filename + fileExtension);
                            hfFileID.Value = fullPath;
                            hfFilePath.Value = fullPath;
                            Page.Request.Files[i].SaveAs(fullPath);


                        }
                        else
                        {

                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    if (fileExtension != ".csv" || fileExtension != ".xls" || fileExtension != ".xlsx")
                    {
                        uFIles.Add(new UploadedFiles { FileName = fullPath });

                    }
                }
                Response.Write(JsonSerialize(uFIles));
                Response.End();

            }
        }
        #endregion

        #region ["Page Control Event"]
        protected void btnMapFields_Click(object sender, EventArgs e)
        {
            ShowExcel(hfFilePath.Value);
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string[] fieldMappings = null;
            if (grdField.Rows.Count > 0)
            {
                fieldMappings = new string[grdField.Rows.Count];
                int fieldCounter = 0;
                foreach (GridViewRow gridItem in grdField.Rows)
                {
                    DropDownList dl = (DropDownList)gridItem.FindControl("ddlDbFields");
                    if (dl != null)
                    {
                        fieldMappings[fieldCounter] = dl.SelectedValue.ToString();
                        fieldCounter++;
                    }
                }
            }
            DataTable dtContacts = GetContactTableStructure();
            string path;
            string connectionString;
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            OleDbDataAdapter da = new OleDbDataAdapter();
            try
            {
                path = hfFilePath.Value;// hfFileID.Value;
                string fileExtension = Path.GetExtension(path);
                DataSet objDataset = new DataSet();

                if (fileExtension.ToUpper() == ".CSV")
                {
                    objDataset = ReadCsvFile(path);

                }
                if (fileExtension.ToUpper() == ".XLS" || fileExtension.ToUpper() == ".XLSX")
                {
                    objDataset = ReadXlsFile(path);
                }
                if (objDataset != null && objDataset.Tables.Count > 0)
                {
                    dt = objDataset.Tables[0];
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            DataRow drNew;
            foreach (DataRow dr in dt.Rows)
            {
                drNew = dtContacts.NewRow();

                for (int fldcnt = 0; fldcnt < fieldMappings.Length; fldcnt++)
                {
                    if (fieldMappings[fldcnt] != "" && fieldMappings[fldcnt] != null && fieldMappings[fldcnt].ToLower() != "ignore")//&& 
                    {
                        // Modified By ItechSoft
                        // Modified on 14/10/1016
                        #region ItechSoft
                        if (fieldMappings[fldcnt] == FieldsEnum.FullName.ToString())
                        {
                            var fullName = Convert.ToString(dr[fldcnt]);
                            if (!string.IsNullOrEmpty(fullName))
                            {
                                if (fullName.Contains(" "))
                                {
                                    var arrName = fullName.Split(' ');
                                    drNew[FieldsEnum.FirstName.ToString()] = arrName[0];
                                    if (arrName.Length > 2)
                                    {
                                        string[] target = new string[arrName.Length - 1];
                                        Array.Copy(arrName, 1, target, 0, arrName.Length - 1);
                                        drNew[FieldsEnum.LastName.ToString()] = string.Join(" ", target);
                                    }
                                    else
                                    {
                                        drNew[FieldsEnum.LastName.ToString()] = arrName[1];
                                    }

                                }
                                else
                                {
                                    drNew[FieldsEnum.FirstName.ToString()] = fullName;
                                }
                            }
                        }
                        else if (fieldMappings[fldcnt] == FieldsEnum.HomeFullAddress.ToString())
                        {
                            var fullAddress = Convert.ToString(dr[fldcnt]).Trim();
                            if (!string.IsNullOrEmpty(fullAddress))
                            {
                                #region ItechSoft 27/10/2016
                                var address = ParseAddress(fullAddress);
                                drNew[FieldsEnum.HomeAddressOne.ToString()] = address.AddressOne;
                                drNew[FieldsEnum.HomeAddressTwo.ToString()] = address.AddressTwo;
                                drNew[FieldsEnum.HomeCity.ToString()] = address.City;
                                drNew[FieldsEnum.HomeState.ToString()] = address.State;
                                //drNew[FieldsEnum..ToString()] = address.Country;
                                drNew[FieldsEnum.HomeZip.ToString()] = address.ZipCode;

                                #endregion
                            }
                        }
                        else if (fieldMappings[fldcnt] == FieldsEnum.BusinessFullAddress.ToString())
                        {
                            var fullAddress = Convert.ToString(dr[fldcnt]).Trim();
                            if (!string.IsNullOrEmpty(fullAddress))
                            {
                                #region ItechSoft 27/10/2016

                                var address = ParseAddress(fullAddress);
                                drNew[FieldsEnum.BusinessAddressOne.ToString()] = address.AddressOne;
                                drNew[FieldsEnum.BusinessAddressTwo.ToString()] = address.AddressTwo;
                                drNew[FieldsEnum.BusinessCity.ToString()] = address.City;
                                drNew[FieldsEnum.BusinessState.ToString()] = address.State;
                                drNew[FieldsEnum.BusinessCounty.ToString()] = address.Country;
                                drNew[FieldsEnum.BusinessZip.ToString()] = address.ZipCode;

                                #endregion
                            }
                        }
                        #region ItechSoft 04/11/2016
                        else if (fieldMappings[fldcnt] == FieldsEnum.OtherFullAddress.ToString())
                        {
                            var fullAddress = Convert.ToString(dr[fldcnt]).Trim();
                            if (!string.IsNullOrEmpty(fullAddress))
                            {
                                var address = ParseAddress(fullAddress);
                                drNew[FieldsEnum.OtherAddressOne.ToString()] = address.AddressOne;
                                drNew[FieldsEnum.OtherAddressTwo.ToString()] = address.AddressTwo;
                                drNew[FieldsEnum.OtherAddressCity.ToString()] = address.City;
                                drNew[FieldsEnum.OtherAddressState.ToString()] = address.State;
                                drNew[FieldsEnum.OtherAddressCountry.ToString()] = address.Country;
                                drNew[FieldsEnum.OtherAddressPostalCode.ToString()] = address.ZipCode;

                            }
                        }
                        #endregion
                        else
                        {
                            #region ItechSoft 05/11/2016

                            if (fieldMappings[fldcnt] == "Birthday")
                            {
                                DateTime bdate;
                                if (DateTime.TryParse(Convert.ToString(dr[fldcnt]), out bdate))
                                {
                                    drNew[fieldMappings[fldcnt]] = dr[fldcnt];
                                }
                            }
                            else if (fieldMappings[fldcnt] == "Anniversary")
                            {
                                DateTime bdate;
                                if (DateTime.TryParse(Convert.ToString(dr[fldcnt]), out bdate))
                                {
                                    drNew[fieldMappings[fldcnt]] = dr[fldcnt];
                                }
                            }

                            else
                            {
                                drNew[fieldMappings[fldcnt]] = dr[fldcnt];
                            }
                            #endregion
                        }

                        #endregion
                    }
                }
                for (int fldcnt = 0; fldcnt < drNew.Table.Columns.Count; fldcnt++)
                {
                    if (drNew[fldcnt].ToString() == "" || drNew[fldcnt].ToString() == "ignore")//|| drNew[fldcnt].ToString() == "ignore"
                    {
                        drNew[fldcnt] = "";
                    }
                }
                dtContacts.Rows.Add(drNew);
            }

            // Added By ItechSoft
            // Added on 14/10/1016
            #region ItechSoft
            dtContacts.Columns.Remove(FieldsEnum.FullName.ToString());
            dtContacts.Columns.Remove(FieldsEnum.HomeFullAddress.ToString());
            dtContacts.Columns.Remove(FieldsEnum.BusinessFullAddress.ToString());
            #endregion


            // Added By ItechSoft
            // Added on 04/11/1016
            dtContacts.Columns.Remove(FieldsEnum.OtherFullAddress.ToString());



            dtContacts.AsEnumerable().ToList<DataRow>()
.ForEach(r => { r["HPrimeID"] = 1; r["GroupId"] = 1; r["HomePhone"] = RemoveSpecialCharacters(r["HomePhone"].ToString()); r["HomePhoneTwo"] = RemoveSpecialCharacters(r["HomePhoneTwo"].ToString()); r["MobilePhone"] = RemoveSpecialCharacters(r["MobilePhone"].ToString()); r["BusinessPhone"] = RemoveSpecialCharacters(r["BusinessPhone"].ToString()); r["BusinessPhoneTwo"] = RemoveSpecialCharacters(r["BusinessPhoneTwo"].ToString()); r["Notes"] = RemoveSpecialCharactersRegEx(r["Notes"].ToString()); });

            ds.Tables.Add(dtContacts);
            string xmlData = ds.GetXml();
            ImportContactsFromFile(xmlData);

            // Added by ItechSoft
            // Added on 15/10/2016
            Response.Redirect("~/CMS_AddressBook.aspx");
        }

        // Added by ItechSoft
        private Address ParseAddress(string fullAddress)
        {
            var address = new Address();
            try
            {
                var arrName = new List<string>();
                string addressOne = string.Empty;
                string addressTwo = string.Empty;
                string zipCode = string.Empty;
                string country = string.Empty;
                string state = string.Empty;
                string city = string.Empty;
                for (int i = fullAddress.Length; i >= 0; i--)
                {
                    if (Char.IsDigit(fullAddress[i - 1]))
                    {
                        zipCode += fullAddress[i - 1];
                    }

                    if (!string.IsNullOrEmpty(zipCode))
                    {
                        if (Char.IsWhiteSpace(fullAddress[i - 1]))
                        {
                            break;
                        }
                    }
                }

                if (!string.IsNullOrEmpty(zipCode))
                {
                    char[] charArray = zipCode.ToCharArray();
                    Array.Reverse(charArray);
                    zipCode = new string(charArray);

                    var lastIndex = fullAddress.LastIndexOf(zipCode);
                    if (lastIndex != -1)
                    {
                        country = fullAddress.Substring(lastIndex + zipCode.Length).Trim();
                        fullAddress = fullAddress.Remove(lastIndex);
                    }
                }
                if (fullAddress.Contains(Environment.NewLine))
                {

                    arrName = fullAddress.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None).ToList();
                    if (arrName.Count > 0)
                    {
                        addressOne = arrName.FirstOrDefault();
                        addressTwo = arrName.Skip(1).FirstOrDefault();
                    }
                }
                if (fullAddress.Contains("\t"))
                {
                    arrName = fullAddress.Split('\t').ToList();
                    if (arrName.Count > 0)
                    {
                        addressOne = arrName.FirstOrDefault();
                        addressTwo = arrName.Skip(1).FirstOrDefault();
                    }
                }
                if (fullAddress.Contains(" "))
                {
                    arrName = fullAddress.Split(' ').ToList();
                    arrName = arrName.Where(s => !string.IsNullOrWhiteSpace(s)).ToList();
                }

                if (arrName.Count > 0)
                {
                    state = arrName.Last();
                    arrName.Remove(state);
                }
                if (arrName.Count > 1)
                {
                    city = string.Join(" ", arrName.Skip(arrName.Count - 2));
                    arrName.RemoveRange(arrName.Count - 2, 2);
                }
                if (arrName.Count > 0)
                {
                    addressOne = string.Join(" ", arrName.ToArray());
                }

                address.AddressOne = addressOne;
                address.AddressTwo = addressTwo;
                address.City = city;
                address.State = state;
                address.Country = country;
                address.ZipCode = zipCode;
                return address;
            }
            catch
            {
                return address;
            }
        }

                        #endregion

        #region["User Define function"]

        private void ImportContactsFromFile(string xmlData)
        {
            using (SqlCommand cmd = new SqlCommand("CRM_ImportContactsFromFile"))
            {
                cmd.Parameters.Add("@XMLString", SqlDbType.Text).Value = xmlData;

                // Added By ItechSoft
                // Added On 14/10/2016
                cmd.Parameters.Add("@HPrimeID", SqlDbType.Int).Value = 1;
                ExecuteNonQuery(cmd);
            }
        }

        private int ExecuteNonQuery(SqlCommand command)
        {
            int result = 0;
            string connectionString = ConfigurationManager.AppSettings["connectionString"].ToString();
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand();
            try
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Connection = con;
                #region ItechSoft 04/11/2016
                command.CommandTimeout = 600;
                #endregion
                con.Open();
                result = command.ExecuteNonQuery();
                lblMessage.Text = "Data Uploaded Successfully ....!!!!";
                return result;
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            finally
            {
                con.Close();
            }

        }

        private DataTable GetContactTableStructure()
        {
            using (DataTable dtContact = new DataTable())
            {
                dtContact.Columns.Add("customerDataID");
                dtContact.Columns.Add("fkUserID");
                dtContact.Columns.Add("FirstName");
                dtContact.Columns.Add("MiddleName");
                dtContact.Columns.Add("LastName");
                dtContact.Columns.Add("SpouseName");
                dtContact.Columns.Add("Title");
                dtContact.Columns.Add("Suffix");
                dtContact.Columns.Add("Initials");
                dtContact.Columns.Add("WebPage");
                dtContact.Columns.Add("Birthday");
                dtContact.Columns.Add("Anniversary");
                dtContact.Columns.Add("Notes");
                dtContact.Columns.Add("EmailAddress");
                dtContact.Columns.Add("Email2Address");
                dtContact.Columns.Add("Email3Address");
                dtContact.Columns.Add("HomePhone");
                dtContact.Columns.Add("HomePhoneTwo");
                dtContact.Columns.Add("HomeFax");
                dtContact.Columns.Add("MobilePhone");
                dtContact.Columns.Add("HomeAddressOne");
                dtContact.Columns.Add("HomeAddressTwo");
                dtContact.Columns.Add("HomeCity");
                dtContact.Columns.Add("HomeState");
                dtContact.Columns.Add("HomeZip");
                dtContact.Columns.Add("BusinessPhone");
                dtContact.Columns.Add("BusinessPhoneTwo");
                dtContact.Columns.Add("BusinessAddressOne");
                dtContact.Columns.Add("BusinessAddressTwo");
                dtContact.Columns.Add("BusinessFax");
                dtContact.Columns.Add("BusinessCity");
                dtContact.Columns.Add("BusinessState");

                // Modified By ItechSoft
                // Modified on 14/10/1016
                dtContact.Columns.Add("BusinessZip");


                dtContact.Columns.Add("BusinessCounty");
                //dtContact.Columns.Add("Categories");
                dtContact.Columns.Add("HPrimeID");
                dtContact.Columns.Add("GroupId");


                // Added By ItechSoft
                // Added on 14/10/1016
                #region ITechSoft 22/10/2016
                dtContact.Columns.Add("FullName");
                dtContact.Columns.Add("HomeFullAddress");
                dtContact.Columns.Add("BusinessFullAddress");
                #endregion

                #region ItechSoft 04/11/2016

                dtContact.Columns.Add("HomeCounty");
                dtContact.Columns.Add("OtherAddressOne");
                dtContact.Columns.Add("OtherAddressTwo");
                dtContact.Columns.Add("OtherAddressCity");
                dtContact.Columns.Add("OtherAddressState");
                dtContact.Columns.Add("OtherAddressCountry");
                dtContact.Columns.Add("OtherAddressPhone");
                dtContact.Columns.Add("OtherAddressPhoneTwo");
                dtContact.Columns.Add("OtherAddressPostalCode");
                dtContact.Columns.Add("OtherAddressFax");
                dtContact.Columns.Add("OtherFullAddress");
                dtContact.Columns.Add("MobilePhoneTwo");
                dtContact.Columns.Add("OtherPhone");
                dtContact.Columns.Add("Email4Address");
                dtContact.Columns.Add("Email5Address");
                dtContact.Columns.Add("Spouse");
                dtContact.Columns.Add("Children");
                dtContact.Columns.Add("Company");
                dtContact.Columns.Add("Gender");
                dtContact.Columns.Add("Profession");
                #endregion

                return dtContact;
            }
        }

        public DataSet ReadCsvFile(string filePath)
        {

            DataSet objDataset1 = new DataSet();
            var connectionString = string.Format(
                @"Provider=Microsoft.ACE.OLEDB.12.0; Data Source={0};Extended Properties=""Text;HDR=YES;FMT=Delimited""",
                Path.GetDirectoryName(filePath)
            );
            using (var objConn = new OleDbConnection(connectionString))
            {
                objConn.Open();
                var strConString = "SELECT * FROM [" + Path.GetFileName(filePath) + "]";
                using (var objAdapter1 = new OleDbDataAdapter(strConString, objConn))
                {
                    objDataset1 = new DataSet("CSV File");
                    objAdapter1.Fill(objDataset1);
                }
                objConn.Close();
            }

            return objDataset1;

        }

        public DataSet ReadXlsFile(string filePath)
        {
            // string connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties=Excel 12.0;";
            string connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2;\"";

            using (OleDbConnection objConn = new OleDbConnection(connectionString))
            {
                objConn.Open();
                DataTable dbSchema = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                if (dbSchema == null || dbSchema.Rows.Count < 1)
                {
                    throw new Exception("Error: Could not determine the name of the first worksheet.");
                }
                string firstSheetName = dbSchema.Rows[0]["TABLE_NAME"].ToString();
                string strConString = "SELECT * FROM [" + firstSheetName + "]";
                using (OleDbCommand objCmdSelect = new OleDbCommand(strConString, objConn))
                {
                    using (OleDbDataAdapter objAdapter1 = new OleDbDataAdapter())
                    {
                        objAdapter1.SelectCommand = objCmdSelect;
                        using (DataSet objDataset1 = new DataSet())
                        {
                            objAdapter1.Fill(objDataset1, "ExcelData");
                            objConn.Close();
                            return objDataset1;
                        }

                    }

                }
            }
        }

        public void ShowExcel(string filePath)
        {
            try
            {
                using (DataTable dtContacts = GetContactTableStructure())
                {
                    string strmail = string.Empty;
                    DataSet objDataset1 = new DataSet();
                    string fileExtension = Path.GetExtension(filePath).ToUpper();
                    if (fileExtension == ".CSV")
                    {
                        objDataset1 = ReadCsvFile(filePath);
                    }
                    if (fileExtension == ".XLS" || fileExtension == ".XLSX")
                    {
                        objDataset1 = ReadXlsFile(filePath);
                    }
                    if (objDataset1 != null && objDataset1.Tables.Count > 0)
                    {
                        if (objDataset1.Tables[0].Rows.Count > 0)
                        {
                            string[] columns = new string[objDataset1.Tables[0].Columns.Count];
                            DropDownList[] ddldb = new DropDownList[objDataset1.Tables[0].Columns.Count];
                            using (DataTable dtFields = new DataTable())
                            {
                                dtFields.Columns.Add("FieldName");
                                dtFields.Columns.Add("RecordName");
                                dtFielsName.Columns.Add("FieldName");
                                DataRow drField;
                                DataRow dr;
                                for (int fldCount = 0; fldCount < objDataset1.Tables[0].Columns.Count; fldCount++)
                                {
                                    dr = dtFields.NewRow();

                                    dr[0] = objDataset1.Tables[0].Columns[fldCount].ColumnName.ToString();
                                    drField = dtFielsName.NewRow();
                                    for (int contact = 2; contact < dtContacts.DefaultView.Table.Columns.Count; contact++)
                                    {
                                        string name1 = dtContacts.Columns[contact].ColumnName.ToString();
                                        string keyList = GetKeyList(name1);
                                        if (keyList != "")
                                        {

                                            int count = 0;
                                            string[] checkKey = keyList.Split(',');
                                            foreach (string strCheckKey in checkKey)
                                            {
                                                if (string.Equals(strCheckKey.ToUpper().Trim(), ((objDataset1.Tables[0].Columns[fldCount].ColumnName.ToString())).ToUpper().Trim(), StringComparison.OrdinalIgnoreCase))// //if (strCheckKey.ToUpper().Trim() == (objDataset1.Tables[0].Columns[fldCount].ColumnName.ToString()).ToUpper().Trim())
                                                {
                                                    isCheckHeader = true;
                                                    keyList = "";
                                                    count++;
                                                    drField[0] = name1;
                                                    break;
                                                }
                                            }
                                            if (count > 0)
                                                break;
                                        }
                                        else
                                        {

                                            drField[0] = "";
                                        }
                                    }
                                    dtFielsName.Rows.Add(drField);

                                    if (objDataset1.Tables[0].Rows[0][fldCount].ToString() == "")
                                    {
                                        for (int rowCount = 1; rowCount < objDataset1.Tables[0].Rows.Count; rowCount++)
                                        {
                                            if (objDataset1.Tables[0].Rows[rowCount][fldCount].ToString() != "")
                                            {
                                                dr[1] = objDataset1.Tables[0].Rows[rowCount][fldCount].ToString();
                                                break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        dr[1] = objDataset1.Tables[0].Rows[0][fldCount].ToString();
                                    }
                                    dtFields.Rows.Add(dr);

                                }

                                grdField.DataSource = dtFields;
                                grdField.DataBind();
                                btnImport.Visible = true;
                                grdField.Visible = true;
                                if (!isCheckHeader)
                                {
                                    lblMessage.Text = "Incorrect file or file format. Please check uploaded file......!!! ";
                                    lblMessage.ForeColor = System.Drawing.Color.Red;
                                    btnImport.Enabled = false;
                                }
                                else
                                {
                                    btnImport.Enabled = true;
                                }
                            }
                        }
                    }
                    //objConn.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string GetKeyList(string strKey)
        {
            using (SqlCommand cmd = new SqlCommand("GetKeyList"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                string strKeyList = GetConfigValue(strKey);
                return strKeyList;
            }
        }

        private string GetConfigValue(string keyName)
        {

            string strValue = string.Empty;
            try
            {
                strValue = System.Configuration.ConfigurationManager.AppSettings[keyName].ToString();
            }
            catch (Exception ex)
            {
                strValue = string.Empty;
            }
            return strValue;
        }

        protected void veriProgressStatLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //((DropDownList)sender).SelectedValue = grdField.DataKeys[((GridViewRow)((DropDownList)sender).Parent.Parent).RowIndex].Values[1].ToString();
            }
            else
            {
                try
                {
                    if (dtFielsName.Rows[countNo][0].ToString() == "")
                    {
                        ((DropDownList)sender).BackColor = ColorTranslator.FromHtml("#f39c11"); //Color.FromName("#f39c11");
                        ((DropDownList)sender).ForeColor = Color.White;// .Attributes.Add("style", "color:white");
                    }
                    ((DropDownList)sender).SelectedValue = dtFielsName.Rows[countNo][0].ToString();

                    countNo++;
                    //BindDropdown(((DropDownList)sender));
                    //base.OnPreRender(e);
                    //((DropDownList)sender).ReorderAlphabetized();
                }
                catch (Exception ex)
                {

                }
            }
        }



        [WebMethod]
        public static string mapField(string filename)
        {
            PResponse pr = new PResponse();
            pr.Code = 2;
            pr.Message = Guid.NewGuid().ToString();
            return JsonSerialize(pr);
        }

        public static string JsonSerialize(object obj)
        {
            var jsonSerializer = new DataContractJsonSerializer(obj.GetType());
            string returnValue = string.Empty;
            using (var memoryStream = new MemoryStream())
            {
                using (var jsonWriter = JsonReaderWriterFactory.CreateJsonWriter(memoryStream))
                {
                    jsonSerializer.WriteObject(jsonWriter, obj);
                    jsonWriter.Flush();
                    returnValue = Encoding.UTF8.GetString(memoryStream.GetBuffer(), 0, (int)memoryStream.Length);
                }
            }
            return returnValue;
        }
        #endregion

        #region ["Property class"]

        public class UploadedFiles
        {
            public string FileName { get; set; }
        }

        public class PResponse
        {
            public int Code { get; set; }
            public string Message { get; set; }
        }

        #endregion

        // Added by ItechSoft
        // Added on 13/10/2016
        // Modified on 15/10/2016

        #region ItechSoft 14/10/2016

        public static string RemoveSpecialCharacters(string str)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in str)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '.' || c == '-' || c == '(' || c == ')' || c == '+')
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }

        public string RemoveSpecialCharactersRegEx(string str)
        {
            return Regex.Replace(str, "[^0-9A-Za-z/\\ ,]", " ", RegexOptions.Compiled);
        }

        private void BindDropdown(DropDownList ddl)
        {

            List<KeyValuePair<string, string>> lst = new List<KeyValuePair<string, string>>();
            if (ViewState["DropDownFieldItems"] == null)
            {
                foreach (FieldsEnum r in Enum.GetValues(typeof(FieldsEnum)))
                {
                    lst.Add(new KeyValuePair<string, string>(Enum.GetName(typeof(FieldsEnum), r), CommonHelper.GetDescriptionFromEnumValue(r)));
                }
                lst = lst.OrderBy(x => x.Value).ToList();
                lst.Insert(0, new KeyValuePair<string, string>("", "Select"));
                lst.Insert(1, new KeyValuePair<string, string>("ignore", "IGNORE"));
                ViewState["DropDownFieldItems"] = lst;
            }
            else
            {
                lst = (List<KeyValuePair<string, string>>)ViewState["DropDownFieldItems"];
            }
            ddl.DataSource = lst;
            ddl.SelectedValue = "";
            ddl.DataTextField = "Value";
            ddl.DataValueField = "Key";
            ddl.DataBind();
        }
        protected void grdField_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            var dropdown = ((DropDownList)e.Row.FindControl("ddlDbFields"));
            if (dropdown != null)
                BindDropdown(dropdown);
        }

        #endregion

        #region ItechSoft 15/10/2016
        protected void ddlDbFields_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList ddlDbFields = (DropDownList)sender;

                //if (ddlDbFields.SelectedValue == "" || ddlDbFields.SelectedValue.ToLower() == "ignore") return;
                var items = new List<KeyValuePair<string, string>>();
                if (ViewState["DropDownFieldItems"] != null)
                {
                    items = (List<KeyValuePair<string, string>>)ViewState["DropDownFieldItems"];
                }

                GridViewRow row = (GridViewRow)ddlDbFields.Parent.Parent;
                int idx = row.RowIndex;

                foreach (GridViewRow gvRow in grdField.Rows)
                {
                    if (gvRow.RowIndex != idx)
                    {
                        DropDownList dl = (DropDownList)gvRow.FindControl("ddlDbFields");
                        if (dl != null)
                        {
                            var selectedValue = dl.SelectedValue;
                            dl.Items.Clear();
                            foreach (var item in items)
                            {
                                if (dl.Items.FindByValue(item.Value) == null && item.Value != ddlDbFields.SelectedValue)
                                {
                                    dl.Items.Add(new ListItem(item.Value, item.Key));
                                }
                            }

                            foreach (GridViewRow gvRow2 in grdField.Rows)
                            {
                                DropDownList dl2 = (DropDownList)gvRow2.FindControl("ddlDbFields");
                                var selectedItem = dl2.SelectedItem;
                                if (selectedItem != null && selectedItem.Value != "" && selectedItem.Value.ToLower() != "ignore")
                                {
                                    dl.Items.Remove(selectedItem);
                                }
                            }

                            if (ddlDbFields.SelectedValue != selectedValue || ddlDbFields.SelectedValue == "ignore")
                            {
                                dl.SelectedValue = selectedValue;
                            }
                        }
                    }
                }

                #region ItechSoft 04/11/2016
                if (ddlDbFields.SelectedValue != "" && ddlDbFields.SelectedValue != "IGNORE")
                {
                    ddlDbFields.BackColor = Color.White;
                    ddlDbFields.ForeColor = Color.Blue;
                }
                else
                {
                    ddlDbFields.BackColor = ColorTranslator.FromHtml("#f39c11");
                    ddlDbFields.ForeColor = Color.White;
                }
                #endregion
            }
            catch
            {

            }
        }

        #endregion
    }
}