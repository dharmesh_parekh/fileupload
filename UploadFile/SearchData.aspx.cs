﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Web.Services;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Drawing;
using System.Data.Sql;
using System.Configuration;
using System.Runtime.Serialization.Json;
using System.Text;


namespace FileUpload
{
    public partial class SearchData : System.Web.UI.Page
    {
        #region ["Page Load Event"]
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                GetData();
        }
        #endregion

        #region "[Aspire Software Consultancy 07/09/2015  Get Data Method]"

        private void GetData()
        {
            try
            {
                DataTable dtdata = new DataTable();
                string connectionString = ConfigurationManager.AppSettings["connectionString"].ToString();
                SqlConnection con = new SqlConnection(connectionString);
                SqlCommand cmd = new SqlCommand("CRM_Getdata", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dtdata);
                if (dtdata.Rows.Count > 0)
                {
                    grdField.DataSource = dtdata;
                    grdField.DataBind();
                }

            }

            catch (Exception ex)
            {
                throw ex;
            }


        }

        protected void grdField_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdField.PageIndex = e.NewPageIndex;
            GetData();
        }



        #endregion
        
    }
}